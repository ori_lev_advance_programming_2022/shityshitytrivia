#pragma once
#include "Room.h"
#include "Game.h"
#include <mutex>

class GameManager
{
public:
	static GameManager& getInstance(IDatabase* db);

	GameManager(const GameManager&) = delete;
	void operator=(const GameManager&) = delete;

	Game* createGame(Room game_room);
	Game* getGame(std::string user_name);
	void deleteGame(unsigned int room_id);

private:
	GameManager(IDatabase* db);

	std::mutex m_game_mutex;
	IDatabase* m_database;
	std::vector<Game*> m_games;
};
