#pragma once	
#include "Question.h"
#include "LoginManager.h"
#include "Room.h"

#include<mutex>
#include<map>

class LogginManager;
class IDatabase;
class Room;

struct GameData
{
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averangeAnswerTime;
	Question currentQuestion;
};

class Game
{
public:
	Game(Room game_room, unsigned int game_id);
	Game(const Game& other);
	~Game();

	Game& operator=(const Game& other);
	
	std::map<std::string, GameData*> getResultes();
	Question getQuestionForUser(std::string user);
	unsigned int getRoomId();
	unsigned int getNumberOfPlayers();

	bool submitAnswer(std::string user, std::string answer, float answer_time);
	void removeUser(std::string user_name);

private:
	unsigned int _number_of_players;
	unsigned int _game_id;
	unsigned int _room_id;
	unsigned int _number_of_questions;
	float _question_answer_time;

	std::mutex m_players_m;
	std::vector<Question> m_questions;
	std::map<std::string, GameData*> m_players;
};
