#pragma once
#include <string>


class IRequestHandler;

struct RequestInfo
{
	int requestId;
	std::string receivalTime;
	std::string buffer;
};

struct RequestResult
{
	std::string buffer;
	IRequestHandler* newHandler;
};


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(const RequestInfo& info) = 0;
	virtual RequestResult handleRequest(const RequestInfo& info) = 0;
private:

};
