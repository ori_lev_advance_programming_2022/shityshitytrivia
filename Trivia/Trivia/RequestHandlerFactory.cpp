#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db) : m_loginManager(LoginManager::getInstance(db)),
m_StatisticsManager(StatisticsManager::getInstance(db)), m_roomManager(RoomManager::getInstance()),
m_gameManager(GameManager::getInstance(db))
{
	m_database = db;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* loginHandler = new LoginRequestHandler(*this);
	return loginHandler;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	MenuRequestHandler* menuHandler = new MenuRequestHandler(m_roomManager, m_StatisticsManager, *this, user);
	return menuHandler;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, unsigned int roomId)
{
	RoomMemberRequestHandler* membarHandler = new RoomMemberRequestHandler(user, m_roomManager, *this, roomId);
	return membarHandler;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, unsigned int roomId)
{
	RoomAdminRequestHandler* handler = new RoomAdminRequestHandler(user, m_roomManager, *this, roomId);
	return handler;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return m_StatisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return m_roomManager;
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Game* game, LoggedUser user)
{
	GameRequestHandler* handler = new GameRequestHandler(game, user, m_gameManager, *this);
	return handler;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return m_gameManager;
}

RequestHandlerFactory& RequestHandlerFactory::getInstance(IDatabase* db)
{
	static RequestHandlerFactory instance(db);
	return instance;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}
