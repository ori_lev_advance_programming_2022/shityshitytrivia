#pragma comment (lib, "ws2_32.lib")
#include "Server.h"
#include <exception>
#include "WSAInitializer.h"
#include <iostream>
#include <bitset>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server& server = Server::getInstance();
		server.run();
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what();
	}
	

	return 0;
}