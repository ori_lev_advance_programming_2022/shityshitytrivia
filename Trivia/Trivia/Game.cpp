#include "Game.h"
#include "SqliteDatabase.h"

Game::Game(Room game_room, unsigned int game_id)
{
	_room_id = game_room.getRoomData()->id;
	_game_id = game_id;
	_number_of_questions = game_room.getRoomData()->numOfQuestionsInGame;
	_question_answer_time = game_room.getRoomData()->timePerQuestion;
	_number_of_players = game_room.getAllUsers().size();

	m_questions = SqliteDatabase::getInstance().getQuestions(game_room.getRoomData()->numOfQuestionsInGame);

	//initializing the players map with the user name and the first question
	std::vector<std::string> players = game_room.getAllUsers();
	for (int i = 0; i < players.size(); i++) {
		GameData* gd = new GameData{ 0,0,0, m_questions[0] };
		m_players.insert(std::pair<std::string, GameData*>({ players[i] }, gd));
	}
		
}

Game::Game(const Game& other)
{
	*this = other;
}

Game::~Game()
{
	std::lock_guard<std::mutex> locker(m_players_m);

	//inserting the statistics of the game for every user
	for (std::pair<std::string, GameData*> keyval : m_players)
	{
		SqliteDatabase::getInstance().insertNewStatistics(*keyval.second, keyval.first, _game_id, _number_of_questions, _question_answer_time);
		delete keyval.second;
	}
}

Game& Game::operator=(const Game& other) 
{	
	m_players.clear();

	for (std::pair<std::string, GameData*> kv : other.m_players) {
		GameData* gd = new GameData(*kv.second);
		m_players.insert(std::pair<std::string, GameData*>({ kv.first }, gd));
	}

	
	m_questions = other.m_questions;

	_room_id = other._room_id;
	_game_id = other._game_id;
	_number_of_questions = other._number_of_questions;
	_question_answer_time = other._question_answer_time;
	_number_of_players = other._number_of_players;
	return *this;
}

std::map<std::string, GameData*> Game::getResultes()
{
	return m_players;
}

Question Game::getQuestionForUser(std::string user)
{
	return m_players[user]->currentQuestion;
}

unsigned int Game::getRoomId()
{
	return _room_id;
}

unsigned int Game::getNumberOfPlayers()
{
	return _number_of_players;
}

bool Game::submitAnswer(std::string user, std::string answer, float answer_time)
{
	bool res = false;

	
	float* avg = &m_players[user]->averangeAnswerTime;
	unsigned int* correct = &m_players[user]->correctAnswerCount;
	unsigned int* wrong = &m_players[user]->wrongAnswerCount;

	*avg = ((*avg * (*correct + *wrong)) + answer_time)/(float)(*correct + *wrong + 1);

	if (m_players[user]->currentQuestion.getCorrectAnswer() == answer) { 
		(*correct) += 1; 
		res = true;
	}
	else { (*wrong) += 1; }

	if (*correct + *wrong < m_questions.size())
		m_players[user]->currentQuestion = m_questions[*correct + *wrong];

	return res;
}

void Game::removeUser(std::string user_name)
{
	//adding to the user stats all the question he havn't answered yet as a wrong answers (moah hahahaha)
	m_players[user_name]->wrongAnswerCount += _number_of_questions - 
		m_players[user_name]->wrongAnswerCount - m_players[user_name]->correctAnswerCount;

	std::lock_guard<std::mutex> locker(m_players_m);
	_number_of_players--;
}
