#include "RoomAdminRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory, unsigned int roomId) :
    m_roomManager(roomManager),
    m_handlerFactory(handlerFactory),
    m_user(user)
{
    std::vector<Room*> rooms = m_roomManager.getRooms();

    for (int i = 0; i < rooms.size(); i++)
    {
        if (roomId == rooms[i]->getRoomData()->id)
        {
            m_room = rooms[i];
            break;
        }
    }
}

bool RoomAdminRequestHandler::isRequestRelevant(const RequestInfo& info)
{
    return info.requestId == GET_ROOM_STATE_CODE ||
        info.requestId == CLOSE_ROOM_CODE ||
        info.requestId == START_GAME_CODE;
}

RequestResult RoomAdminRequestHandler::handleRequest(const RequestInfo& info)
{
    RequestResult result;

    switch (info.requestId)
    {
    case GET_ROOM_STATE_CODE:
        result = getRoomState(info);
        break;

    case CLOSE_ROOM_CODE:
        result = closeRoom(info);
        break;

    case START_GAME_CODE:
        result = startGame(info);
        break;
    default:
        result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, m_room->getRoomData()->id);
        result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
        break;
    }

    return result;
}

RequestResult RoomAdminRequestHandler::closeRoom(const RequestInfo& info)
{
    RequestResult result;

    m_roomManager.deleteRoom(m_room->getRoomData()->id);

    result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse{ SUCCESS_CODE });

    return result;
}

RequestResult RoomAdminRequestHandler::startGame(const RequestInfo& info)
{
    RequestResult result;
    m_room->getRoomData()->isActive = ACTIVE;

    result.newHandler = m_handlerFactory.createGameRequestHandler(m_handlerFactory.getGameManager().createGame(*m_room), m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(StartGameResponse{ SUCCESS_CODE });
    
    return result;
}

RequestResult RoomAdminRequestHandler::getRoomState(const RequestInfo& info)
{
    RequestResult result;
    RoomState state;
    RoomData data = *m_room->getRoomData();
    
    state.answer_count = data.numOfQuestionsInGame;
    state.answer_timeout = data.timePerQuestion;
    state.has_game_begun = m_roomManager.getRoomState(data.id);
    state.players = m_room->getAllUsers();
    state.status = SUCCESS_CODE;

    result.buffer = JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{ SUCCESS_CODE, state });

    result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, data.id);

    return result;
}
