#include "RoomManager.h"

RoomManager::RoomManager()
{
	last_room_id = 0;
}

RoomManager& RoomManager::getInstance()
{
	static RoomManager instance;
	return instance;
}

void RoomManager::createRoom(LoggedUser& owner, RoomData& data)
{
	data.id = ++last_room_id;
	Room room = Room(data);

	room.addUser(owner);
	std::lock_guard<std::mutex> lock(m_locker);
	m_rooms.insert({ data.id, room });
}

void RoomManager::deleteRoom(int ID)
{
	std::lock_guard<std::mutex> lock(m_locker);
	m_rooms.erase(ID);
}

unsigned int RoomManager::getRoomState(int ID)
{
	std::lock_guard<std::mutex> lock(m_locker);
	return m_rooms[ID].getRoomData()->isActive;
}

std::vector<Room*> RoomManager::getRooms()
{
	std::vector<Room*> data;

	std::lock_guard<std::mutex> lock(m_locker);
	if (m_rooms.size() == 0)
		return data;

	for (std::map<int, Room>::iterator i = m_rooms.begin(); i != m_rooms.end(); ++i)
			data.emplace_back(&i->second);

	return data;
}
