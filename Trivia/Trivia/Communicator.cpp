#include "Communicator.h"
#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

#include <ctime>
#include <string>
#include <chrono>
#include <format>

#define PORT 60000
#define MSG_CODE 1
#define BIG_BUFFER 1000
#define DATA_LENGH_SIZE 11
#define TIME_BUFFER 26

/// <summary>
/// function return the current date and time as a string
/// </summary>
/// <returns></returns>
std::string getCurrentTimeAsStringc()
{
	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	char time[TIME_BUFFER] = { 0 };
	errno_t err = ctime_s(time, TIME_BUFFER, &end_time);
	
	return std::string(time);
}

Communicator::Communicator(RequestHandlerFactory& factory) : m_handlerFactory(factory)
{
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator& Communicator::getInstance(RequestHandlerFactory& factory)
{
	static Communicator instance(factory);
	return instance;
}

Communicator::~Communicator()
{
	for (std::pair<SOCKET, IRequestHandler*> client_pair : m_clients)
		delete client_pair.second;
	
	try
	{
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;

		//accapting clients
		SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception("Faild to accept a client");

		std::cout << "Client accepted. Server and client can speak" << std::endl;

		// creating a client threads 
		std::thread client_thread(&Communicator::handleNewClient, std::ref(*this), std::ref(client_socket));
		client_thread.detach();

		//updating the map
		std::lock_guard<std::mutex> lock(m_map_lock);
		m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, m_handlerFactory.createLoginRequestHandler()));
	}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception("Faild to bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception("Fiald to listen");
	std::cout << "Listening on port " << PORT << std::endl;
}

void Communicator::handleNewClient(SOCKET client_sock)
{
	char code = NULL;
	int size = 0;

	try
	{
		while (true)
		{
			char data_size[BIG_BUFFER] = { 0 };
			char data[BIG_BUFFER] = { 0 };

			//geting the msg code 
			RequestInfo info;
			recv(client_sock, &code, MSG_CODE, 0);
			info.requestId = code;

			//getting the data size as char and casting it to int
			recv(client_sock, data_size, DATA_LENGH_SIZE, 0);
			size = std::stoul(data_size, nullptr, 16);
			
			//getting the data
			if(size != 0)
				recv(client_sock, data, size, 0);
			info.buffer = data;

			info.receivalTime = getCurrentTimeAsStringc();

			//reseting all the buffers to nothing to prevent mess in the casting later.
			code = 0;
			size = 0;
			
			//if the msg from the client is relevnt to the current state of the user
			if (m_clients[client_sock]->isRequestRelevant(info)) {
				//taking care of the client request and getting the respones to the client and the next handler
				RequestResult result = m_clients[client_sock]->handleRequest(info);

				send(client_sock, result.buffer.c_str(), result.buffer.size(), 0);
				
				//replacign the current handler of the user to the new handler
				std::lock_guard<std::mutex> lock(m_map_lock);
				delete m_clients[client_sock];
				m_clients[client_sock] = result.newHandler;
			}
			else {
				//sending an error msg to the client
				std::string msg = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
				send(client_sock, msg.c_str(), msg.size(), 0);
			}

			
		}
	


	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		closesocket(client_sock);

		std::lock_guard<std::mutex> lock(m_map_lock);
		delete m_clients[client_sock];
		m_clients.erase(client_sock);
	}
}
