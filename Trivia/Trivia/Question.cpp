#include "Question.h"

Question::Question(std::string question, std::vector<std::string> possibleAnswers) : m_question(question), 
	m_possibleAnswers(possibleAnswers) {}

std::string Question::getQuestion()
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return m_possibleAnswers[0];
}

