#pragma once
#include "IDatabase.h"
#include "Response.h"


class StatisticsManager
{
public:
	static StatisticsManager& getInstance(IDatabase* db);

	StatisticsManager(const StatisticsManager&) = delete;
	void operator=(const StatisticsManager&) = delete;

	std::vector<GameSummary> getUsersLastGames(const std::string& user_name);
	std::vector<GameSummary> getHighScore();
	Stats getUserStatistics(const std::string& user_name);

private:
	StatisticsManager(IDatabase* db);

	std::string convertGameSummaryToString(const GameSummary& game);

	IDatabase* m_database;
};

