#include "GameRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

#define NO_ANSWER 9

GameRequestHandler::GameRequestHandler(Game* game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFacroty) :
    m_gameManager(gameManager),
    m_handlerFacroty(handlerFacroty),
    m_user(user),
    m_game(game)
{
   // m_game = new Game(game);
}

bool GameRequestHandler::isRequestRelevant(const RequestInfo& info)
{
    return info.requestId == LEAVE_GAME_CODE ||
        info.requestId == GET_QUESTIONS_CODE ||
        info.requestId == SUBMIT_ANSWER_CODE ||
        info.requestId == GET_GAME_RESULTS_CODE;
}

RequestResult GameRequestHandler::handleRequest(const RequestInfo& info)
{
    RequestResult result;

    switch (info.requestId)
    {
    case LEAVE_GAME_CODE:
        result = leaveGame(info);
        break;

    case GET_QUESTIONS_CODE:
        result = getQuestion(info);
        break;

    case SUBMIT_ANSWER_CODE:
        result = submitAnswer(info);
        break;

    case GET_GAME_RESULTS_CODE:
        result = getGameResults(info);
        break;

    default:
        result.newHandler = m_handlerFacroty.createGameRequestHandler(m_game, m_user);
        result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
        break;
    }
    return result;
}

RequestResult GameRequestHandler::getQuestion(const RequestInfo& info)
{
    RequestResult result;

    Question q = m_game->getQuestionForUser(m_user.m_username);

    result.buffer = JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse{ SUCCESS_CODE, q.getQuestion(), q.getPossibleAnswers()});
    result.newHandler = m_handlerFacroty.createGameRequestHandler(m_game, m_user);

    return result;
}

RequestResult GameRequestHandler::submitAnswer(const RequestInfo& info)
{
    RequestResult result;
    
    SubmitAnswerRequest req = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(info.buffer);

    std::string answer = "";
    if(req.answerId != NO_ANSWER)
        answer = m_game->getQuestionForUser(m_user.m_username).getPossibleAnswers()[(int)req.answerId];
  
    unsigned int status = m_game->submitAnswer(m_user.m_username, answer, req.answerTime) ? 1 : 0;

    result.newHandler = m_handlerFacroty.createGameRequestHandler(m_game, m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse{status, 0});

    return result;
}

RequestResult GameRequestHandler::getGameResults(const RequestInfo& info)
{
    RequestResult result;

    std::map<std::string, GameData*> data = m_game->getResultes();
    std::vector<PlayerResults> vec;

    
    
    for (std::pair<std::string, GameData*> const& keyValue : data)
    {
        vec.emplace_back(PlayerResults{ keyValue.first,
            keyValue.second->correctAnswerCount,
            keyValue.second->wrongAnswerCount,
            keyValue.second->averangeAnswerTime});
    }
    
    result.newHandler = m_handlerFacroty.createGameRequestHandler(m_game, m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse{SUCCESS_CODE, vec});
    return result;
}

RequestResult GameRequestHandler::leaveGame(const RequestInfo& info)
{
    RequestResult result;
    
    m_game->removeUser(m_user.m_username);
    if (!m_game->getNumberOfPlayers()) { m_gameManager.deleteGame(m_game->getRoomId()); }

    result.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{ SUCCESS_CODE });

    return result;
}
