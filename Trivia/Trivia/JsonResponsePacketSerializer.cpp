#include "JsonResponsePacketSerializer.h"
#include  "json.hpp"
#include "Request.h"
#include <string>
#include <map>


NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Stats, average_answer_time, total_of_games, total_of_answers, total_of_correct_answers, correct_answers_rate)
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GameSummary, finel_score, date, game_id, placment, user_name)
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(RoomState, status, answer_count, answer_timeout, has_game_begun, players)
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PlayerResults, username, correctAnswerCount, wrongAnswerCount, averageAnswerCount)


#define DATA_LENGH_SIZE 4
#define BIG_BUFFER 1000
#define SAPERATOR "|"

#define JSON_KEY_HIGH_SCORE  "HighScores"
#define JSON_KEY_LAST_GAMES "LastGames"
#define JSON_KEY_PLAYERS_IN_ROOM "PlayersInRoom"
#define JSON_KEY_ROOMS "Rooms"

#define NO_CONTENT_MSG "There are no content to show right for now. try again later"

using json = nlohmann::json;

/// <summary>
/// function gets the data of the dst msg and building a full msg (code, data size, data)
/// </summary>
/// <param name="response"></param>
/// <returns></returns>
std::string buildBuffer(int code, const std::string& data)
{
    int data_size = (int)data.size();
    char buffer[BIG_BUFFER] = { 0 };

    //casting the code and the data size from int into chars
    buffer[0] = code;
    memcpy(buffer + 1, (char*)&data_size, DATA_LENGH_SIZE);

    //std::reverse(buffer + 1, buffer + 5);

    //adding the data to the buffer
    strncpy(buffer + 1 + DATA_LENGH_SIZE, data.c_str(), data_size);
    
    return std::string(buffer, buffer + 1 + DATA_LENGH_SIZE + data_size);
}


std::string JsonResponsePacketSerializer::serializeResponse(const ErrorResponse& response)
{
    json j{
        {"message", response.message}
    };

    return buildBuffer(ERROR_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LoginResponse& response)
{   
    json j{
        {"status", response.status}
    };

    return buildBuffer(LOGIN_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const SignupResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(LOGIN_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const JoinRoomResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(JOIN_ROOM_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const CreateRoomResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(CREATE_ROOM_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LogoutResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(LOGOUT_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const CloseRoomResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(CLOSE_ROOM_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const AddQuestionResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(ADD_QUESTION_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LeaveGameResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(LEAVE_GAME_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetQuestionResponse& response)
{
    json room_json_map;

    for (int i = 0; i < response.answers.size(); i++)
        room_json_map.emplace_back(response.answers[i]);


    json j{
        {"status", response.status},
        {"question", response.question},
        {"answer", room_json_map }
    };

    return buildBuffer(GET_QUESTIONS_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const SubmitAnswerResponse& response)
{  
    json j{
        {"status", response.status},
        {"correctAnswerId", response.correctAnswerId}
    };

    return buildBuffer(SUBMIT_ANSWER_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetGameResultsResponse& response)
{
    json j{
        {"status", response.status},
        {"results", response.results}
    };

    return buildBuffer(GET_GAME_RESULTS_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LeaveRoomResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(LEAVE_ROOM_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const StartGameResponse& response)
{
    json j{
        {"status", response.status}
    };

    return buildBuffer(START_GAME_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetRoomStateResponse& response)
{
    json j{
        {"status", response.status},
        {"room_state", response.state}
    };

    return buildBuffer(GET_ROOM_STATE_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const getPersonalStatsResponse& response)
{
    json j{
        {"status", response.status},
        {"UserStatistics", response.statistics}
    };

    return buildBuffer(GET_PERSONAL_STATS_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const getHighScoreResponse& response)
{
    json j{
       {"status", response.status},
       {JSON_KEY_HIGH_SCORE, response.games}
    };
    return buildBuffer(GET_HIGH_SCORE_CODE, j.dump());
}



std::string JsonResponsePacketSerializer::serializeResponse(const getLastGamesOfPlayer& response)
{
    json j{
       {"status", response.status},
       {JSON_KEY_LAST_GAMES, response.last_games}
    };

    return buildBuffer(GET_LAST_GAMES_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetPlayersInRoomResponse& response)
{
    json j{
       {"status", response.status},
       {JSON_KEY_PLAYERS_IN_ROOM, json(response.players)}
    };

    return buildBuffer(GET_PLAYERS_IN_ROOM_CODE, j.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetRoomsResponse& response)
{
    json room_json_map;

    for (int i = 0; i < response.rooms.size(); i++)
        room_json_map.emplace(response.rooms[i]->getRoomData()->name, response.rooms[i]->getRoomData()->id );

    json j{
        {"status", response.status},
        {JSON_KEY_ROOMS, room_json_map}
    };

    return buildBuffer(GET_ROOMS_CODE, j.dump());
}
