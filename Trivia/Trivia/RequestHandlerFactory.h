#pragma once
#include "LoginManager.h"
#include "IDatabase.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "GameManager.h"


class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class GameManager;

class RequestHandlerFactory {
public:
	static RequestHandlerFactory& getInstance(IDatabase* db);

	RequestHandlerFactory(const RequestHandlerFactory&) = delete;
	void operator=(const RequestHandlerFactory&) = delete;

	LoginManager& getLoginManager();

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, unsigned int roomId);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user, unsigned int roomId);
	
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();

	GameRequestHandler* createGameRequestHandler(Game* game, LoggedUser user);
	GameManager& getGameManager();


private:
	RequestHandlerFactory(IDatabase* db);

	LoginManager& m_loginManager;
	IDatabase* m_database;
	RoomManager& m_roomManager;
	StatisticsManager& m_StatisticsManager;
	GameManager& m_gameManager;

};