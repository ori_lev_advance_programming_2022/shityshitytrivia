#pragma once
#include "Response.h"

class JsonResponsePacketSerializer
{
public:
	static std::string serializeResponse(const ErrorResponse& response);
	static std::string serializeResponse(const LoginResponse& response);
	static std::string serializeResponse(const LogoutResponse& response);
	static std::string serializeResponse(const SignupResponse& response);
	static std::string serializeResponse(const getPersonalStatsResponse& response);
	static std::string serializeResponse(const getHighScoreResponse& response);
	static std::string serializeResponse(const getLastGamesOfPlayer& response);
	static std::string serializeResponse(const GetPlayersInRoomResponse& response);
	static std::string serializeResponse(const GetRoomsResponse& response);
	static std::string serializeResponse(const JoinRoomResponse& response);
	static std::string serializeResponse(const CreateRoomResponse& response);
	static std::string serializeResponse(const GetRoomStateResponse& response);
	static std::string serializeResponse(const StartGameResponse& response);
	static std::string serializeResponse(const LeaveRoomResponse& response);
	static std::string serializeResponse(const CloseRoomResponse& response);
	static std::string serializeResponse(const AddQuestionResponse& response);

	static std::string serializeResponse(const LeaveGameResponse& response);
	static std::string serializeResponse(const GetQuestionResponse& response);
	static std::string serializeResponse(const SubmitAnswerResponse& response);
	static std::string serializeResponse(const GetGameResultsResponse& response);
private:
};




