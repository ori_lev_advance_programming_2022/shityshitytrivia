#include "StatisticsManager.h"
#include "json.hpp"

#define TO_PRECENTAGE 100
using json = nlohmann::json;

StatisticsManager::StatisticsManager(IDatabase* db) : m_database(db) 
{
}

std::vector<GameSummary> StatisticsManager::getHighScore()
{
	return m_database->getBestFiveGames();
}

Stats StatisticsManager::getUserStatistics(const std::string& user_name)
{
	int num_of_correct_ans = m_database->getNumOfCorrectAnswers(user_name), total_ans_count = m_database->getNumOfTotalAnswers(user_name);
	float succes_rate = 0;

	Stats ans;

	if (total_ans_count)
		succes_rate = (num_of_correct_ans / (float)total_ans_count) * TO_PRECENTAGE;

	
	ans.correct_answers_rate = 0;
	ans.average_answer_time = m_database->getPlayerAverageAnswerTime(user_name);
	ans.total_of_games = m_database->getNumOfPlayerGames(user_name);
	ans.total_of_answers = total_ans_count;
	ans.total_of_correct_answers = num_of_correct_ans;

	if (total_ans_count != 0)
		ans.correct_answers_rate = ((double)num_of_correct_ans / (double)total_ans_count) * TO_PRECENTAGE;

	return ans;
}

StatisticsManager& StatisticsManager::getInstance(IDatabase* db)
{
	static StatisticsManager instance(db);
	return instance;
}

std::vector<GameSummary> StatisticsManager::getUsersLastGames(const std::string& user_name)
{
	return m_database->getLastFiveGames(user_name);

}

std::string StatisticsManager::convertGameSummaryToString(const GameSummary& game)
{
	json j{
		
		{"game_id", game.game_id},
		{"finel_score", game.finel_score},

		{"user_name", game.user_name},
		{"placment", game.placment},
		{"game_date", game.date}
	};

	return j.dump();
}
