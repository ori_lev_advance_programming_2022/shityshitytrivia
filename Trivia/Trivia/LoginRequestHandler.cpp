#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

// init the factory with ref ctor, and get the login manager from it.
LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& factory) : 
	m_handlerFactory(factory),
	m_loginManager(factory.getLoginManager())
{
}

bool LoginRequestHandler::isRequestRelevant(const RequestInfo& info)
{
	return (LOGIN_CODE == info.requestId || SIGNUP_CODE == info.requestId);
}

RequestResult LoginRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result;

	switch(info.requestId)
	{
		case LOGIN_CODE:
			result = login(info);
			break;
	
		case SIGNUP_CODE:
			result = signup(info);
			break;
		default:
			result.newHandler = m_handlerFactory.createLoginRequestHandler();
			result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
			break;
	}
	return result;
}

// deserialize the request and create quary to the db to log in the user, check if it work and
// return the right result. success/error
RequestResult LoginRequestHandler::login(const RequestInfo& info)
{
	RequestResult result;
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);

	std::string login_resulte = m_loginManager.login(loginReq);
	if (login_resulte.size() == 0)
	{
		LoggedUser user = { loginReq.username };
		result.newHandler = m_handlerFactory.createMenuRequestHandler(user);
		result.buffer = JsonResponsePacketSerializer::serializeResponse(LoginResponse({ SUCCESS_CODE }));
	}
	else
	{
		result.newHandler = m_handlerFactory.createLoginRequestHandler();
		result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ login_resulte }));
	}
	return result;
}

// deserialize the request and create quary to the db to signup the user, check if it work and
// return the right result. success/error
RequestResult LoginRequestHandler::signup(const RequestInfo& info)
{
	RequestResult result;
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer);

	std::string signup_resulte = m_loginManager.signup(signupReq);
	if (!signup_resulte.size())
	{
		LoggedUser user = { signupReq.username };
		result.newHandler = m_handlerFactory.createMenuRequestHandler(user);
		result.buffer = JsonResponsePacketSerializer::serializeResponse(SignupResponse({ SUCCESS_CODE }));
	}
	else
	{
		result.newHandler = m_handlerFactory.createLoginRequestHandler();
		result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ signup_resulte }));
	}
	return result;
}
