#pragma once
#include "Communicator.h"


class Server
{
public:
	static Server& getInstance();

	Server(const Server&) = delete;
	void operator=(const Server&) = delete;

	void run();

private:
	Server();

	IDatabase* m_database;
	RequestHandlerFactory& m_handlerFactory;
	Communicator& m_communicator;

};

