#pragma once

#include <string>
#include <vector>
#include <map>
#include "Room.h"

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};

struct AddQuestionResponse
{
	unsigned int status;
};

struct LogoutResponse
{
	unsigned int status;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<Room*> rooms;
};

struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
};

struct getHighScoreResponse
{
	unsigned int status;
	std::vector<GameSummary> games;
};

struct Stats
{
	double average_answer_time;
	int total_of_games;
	int total_of_answers;
	int total_of_correct_answers;
	double correct_answers_rate;
};

struct getPersonalStatsResponse
{
	unsigned int status;
	Stats statistics;
};

struct getLastGamesOfPlayer
{
	std::vector<GameSummary> last_games;
	unsigned int status;
};

struct RoomState
{
	unsigned int status;
	unsigned int answer_count;
	unsigned int answer_timeout;
	bool has_game_begun;
	std::vector<std::string> players;
};

struct GetRoomStateResponse
{
	unsigned int status;
	RoomState state;
};

struct LeaveGameResponse
{
	unsigned int status;
};

struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::vector<std::string> answers;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned correctAnswerId;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerCount;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
};
