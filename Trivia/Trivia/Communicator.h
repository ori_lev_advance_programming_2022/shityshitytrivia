#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <thread>
#include <map>
#include <iostream>
#include <mutex>

#include "IRequestHandler.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Communicator
{
public:
	static Communicator& getInstance(RequestHandlerFactory& factory);
	~Communicator();

	Communicator(const Communicator&) = delete;
	void operator=(const Communicator&) = delete;

	void startHandleRequests();

private:
	Communicator(RequestHandlerFactory& factory);

	void bindAndListen();
	void handleNewClient(SOCKET client_sock);
		
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;
	std::mutex m_map_lock;
};

