#pragma once

#include "IRequestHandler.h"
#include "Request.h"
#include "RequestHandlerFactory.h"
#include "GameManager.h"

class RequestHandlerFactory;


class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(Game* game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFacroty);

	bool isRequestRelevant(const RequestInfo& info) override;
	RequestResult handleRequest(const RequestInfo& info) override;

private:
	RequestResult getQuestion(const RequestInfo& info);
	RequestResult submitAnswer(const RequestInfo& info);
	RequestResult getGameResults(const RequestInfo& info);
	RequestResult leaveGame(const RequestInfo& info);


	Game* m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFacroty;

};
