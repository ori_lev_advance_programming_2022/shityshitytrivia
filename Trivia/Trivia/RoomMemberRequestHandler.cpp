#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include <iostream>

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory, unsigned int roomId) :
    m_roomManager(roomManager),
    m_handlerFactory(handlerFactory),
    m_user(user),
    m_room_id(roomId)
{}

bool RoomMemberRequestHandler::isRequestRelevant(const RequestInfo& info)
{
    return info.requestId == LEAVE_ROOM_CODE ||
        info.requestId == GET_ROOM_STATE_CODE;
}

RequestResult RoomMemberRequestHandler::handleRequest(const RequestInfo& info)
{
    RequestResult result;

    switch (info.requestId)
    {
    case LEAVE_ROOM_CODE:
        result = leaveRoom(info);
        break;
    case GET_ROOM_STATE_CODE:
        result = getRoomState(info);
        break;
    default:
        result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, m_room_id);
        result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
        break;
    }

    return result;
}

RequestResult RoomMemberRequestHandler::leaveRoom(const RequestInfo& info)
{
    RequestResult result;
    std::vector<Room*> rooms = m_roomManager.getRooms();

    for (int i = 0; i < rooms.size(); i++)
    {
        if (m_room_id == rooms[i]->getRoomData()->id)
        {
            rooms[i]->removeUser(m_user);
            break;
        }
    }

    result.buffer = JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{ SUCCESS_CODE });
    result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(const RequestInfo& info)
{
    RequestResult result;
    RoomState state;

    std::vector<Room*> rooms = m_roomManager.getRooms();
    Room* room = nullptr;

    for (int i = 0; i < rooms.size(); i++)
    {
        if (m_room_id == rooms[i]->getRoomData()->id)
        {
            room = rooms[i];
            break;
        }
    }

    if (room == nullptr)
    {
        state.answer_count = 0;
        state.answer_timeout = 0;
        state.has_game_begun = 0;
        state.players = std::vector<std::string>();
        state.status = ERROR_CODE;

        result.buffer = JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{ ERROR_CODE, state });

        result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
    }
    else {
        RoomData data = *room->getRoomData();

        state.answer_count = data.numOfQuestionsInGame;
        state.answer_timeout = data.timePerQuestion;
        state.has_game_begun = m_roomManager.getRoomState(data.id);
        state.players = room->getAllUsers();
        state.status = SUCCESS_CODE;

        result.buffer = JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{ SUCCESS_CODE, state });

        if (state.has_game_begun)
        {
            result.newHandler = m_handlerFactory.createGameRequestHandler(m_handlerFactory.getGameManager().getGame(m_user.m_username), m_user);
        }
        else
        {
            result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, data.id);
        }
        

        for(int i = 0; i < state.players.size(); i++)
            std::cout << state.players[i];
        std::cout << std::endl;
    }
    

    return result;
}
