#include "GameManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

GameManager::GameManager(IDatabase* db) : m_database(db) {}

GameManager& GameManager::getInstance(IDatabase* db)
{
	static GameManager INSTANCE(db);
	return INSTANCE;
}

Game* GameManager::createGame(Room game_room)
{
	std::lock_guard<std::mutex> locker(m_game_mutex);
	Game* g = new Game(game_room, (unsigned int)m_database->insertNewGame(game_room.getAllUsers()));
	m_games.push_back(g);
	return g;
}

/// <summary>
/// deleting the game with the input room id, as well as removing him from games' vector and the deleting room that
/// created him
/// </summary>
void GameManager::deleteGame(unsigned int room_id)
{
	std::lock_guard<std::mutex> locker(m_game_mutex);
	for (int i = 0; i < m_games.size(); i++)
		if (m_games[i]->getRoomId() == room_id) {
			RequestHandlerFactory::getInstance(m_database).getRoomManager().deleteRoom(room_id);
			Game* g = m_games[i];
			m_games.erase(m_games.begin() + i);
			delete g;
			break;
		}
}

/// <summary>
/// return object of game that contain the inout user name
/// </summary>
Game* GameManager::getGame(std::string user_name)
{
	for (Game* g : m_games)
	{
		std::map<std::string, GameData*> users = g->getResultes();
		if (users.find(user_name) != users.end())
			return g;
	}
}