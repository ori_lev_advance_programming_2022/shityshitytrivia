#pragma once
#include <iostream>
#include <vector>
#include <list>

#include "Question.h"
#include "Game.h"
#include "Request.h"

class Game;
class LogginManager;
class MenuRequsetHandler;
struct GameData;

struct GameSummary
{
	unsigned int game_id;
	float finel_score;

	std::string user_name;
	std::string placment;
	std::string date;
};

class IDatabase
{
public:
	IDatabase() = default;

	IDatabase(const IDatabase&) = delete;
	void operator=(const IDatabase&) = delete;

	virtual bool doesUserExist(const std::string& user_name) = 0;
	virtual bool doesPasswordMatch(const std::string& user_name, const std::string& password) = 0;
	virtual void addNewUser(const std::string& user_name, const std::string& password, const std::string& email, const std::string& addres, const std::string& phone, const std::string& birth_date) = 0;

	virtual float getPlayerAverageAnswerTime(const std::string& user_name) = 0;
	virtual int getNumOfCorrectAnswers(const std::string& user_name) = 0;
	virtual int getNumOfTotalAnswers(const std::string& user_name) = 0;
	virtual int getNumOfPlayerGames(const std::string& user_name) = 0;

	virtual std::vector<GameSummary> getLastFiveGames(const std::string& user_name) = 0;
	virtual std::vector<GameSummary> getBestFiveGames() = 0;

	virtual std::vector<Question> getQuestions(int number_of_questions) = 0;
	virtual void insertNewQuestion(AddQuestionRequest q) = 0;

	virtual void insertNewStatistics(GameData game_data, std::string user_name, unsigned int game_id, unsigned int number_of_q, float q_time) = 0;
	virtual int insertNewGame(std::vector<std::string> users) = 0;
};
