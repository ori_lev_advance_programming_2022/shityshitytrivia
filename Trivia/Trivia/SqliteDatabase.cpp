#include "SqliteDatabase.h"

#include <io.h>
#include <string>

#define EXIST_CODE 1
#define SQL_ERROR_CODE 9
#define EXPECTED_RESPONSE 1
#define SECOND_CHAR 1
#define FULL_PRESENT 100
#define TIME_BUFFER 1000

#define DB_FILE "DBfile.sqlite"
#define DATE_COL "game_date"
#define PLAYERS_COL "players"
#define GAME_ID_COL "game_id"
#define SCORE_COL "game_score"
#define USER_NAME_COL "username"
#define STATS_ID_COL "statistics_id"
#define USER_NAMES_SEPARATOR "|"


/// <summary>
/// function return the current date and time as a string
/// </summary>
/// <returns></returns>
std::string getCurrentTimeAsString()
{
	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	char time[TIME_BUFFER] = { 0 };
	errno_t err = ctime_s(time, TIME_BUFFER, &end_time);

	return std::string(time);
}


/// <summary>
/// this callback work very simply. when i want to check if something exist (in this case if user exist of if there is a 
/// user that match the password), i just ask something from the sql if this thing exist, and if it does exist, the sql will
/// return something and the callback will be called, and he will change the value of the 'res' to true. if it doesn't exist, the
/// sql won't return anything and the callback function won't we called, so the 'res' won't change. 
/// </summary>
int checkIfSomeThingReturnedCallback(void* data, int argc, char** argv, char** azColName)
{
	int* check_result = (static_cast<int*>(data));

	if (argc > EXPECTED_RESPONSE) { *check_result = SQL_ERROR_CODE; }
	else *check_result = EXIST_CODE;
	return 0;
}

//get an int from the db
int intCallback(void* data, int argc, char** argv, char** azColName)
{
	int* amount = (static_cast<int*>(data));

	if(argc != 0 && argv[0] != nullptr)
		*amount = atoi(argv[0]);

	return 0;
}

//get a float from the db
int floatCallback(void* data, int argc, char** argv, char** azColName)
{
	float* stats = (static_cast<float*>(data));

	if (argc != 0 && argv[0] != nullptr)
		*stats = atof(argv[0]);
	return 0;
}

int dateAndPlayersCallback(void* data, int argc, char** argv, char** azColName)
{
	//use pair of date and user names string
	std::pair<std::string, std::string>* info = (static_cast<std::pair<std::string, std::string>*>(data));

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == PLAYERS_COL)
			info->second = argv[i];
		else if (std::string(azColName[i]) == DATE_COL)
			info->first = argv[i];
	}

	return 0;
}

int gameIdAndScoreCallback(void* data, int argc, char** argv, char** azColName)
{
	//use pair of score and id
	std::vector<GameSummary>* stats = (static_cast<std::vector<GameSummary>*>(data));
	GameSummary gs;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "game_id")
			gs.game_id = atoi(argv[i]);
		else if (std::string(azColName[i]) == "game_score")
			gs.finel_score = atof(argv[i]);
		else if (std::string(azColName[i]) == "username")
			gs.user_name = argv[i];
	}

	stats->emplace_back(gs);
	return 0;
}

int questionsCallback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<Question>* res = (static_cast<std::vector<Question>*>(data));
	std::string question, correct_answer;
	std::vector<std::string> answers;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "question") {
			question = argv[i];
		}
		else if (std::string(azColName[i]) == "correct") {
			correct_answer = argv[i];
		}
		else if (std::string(azColName[i]).find("wrong") != std::string::npos) {
			answers.push_back(argv[i]);
		}
	}

	answers.insert(answers.begin(), correct_answer);
	res->push_back(Question(question, answers));
	return 0;
}

/// <summary>
///checking if there is an allready exist db file, and if not, its create new one and initilize him.
/// </summary>
SqliteDatabase::SqliteDatabase()
{
	int doesFileExist = _access(DB_FILE, 0);
	int res = sqlite3_open(DB_FILE, &_sql);
	if (res != SQLITE_OK) { throw std::exception("Error while opening the sql"); }
	if (doesFileExist == -1) { initDB(); }
}

SqliteDatabase& SqliteDatabase::getInstance()
{
	static SqliteDatabase instance;
	return instance;
}

/// <summary>
/// closing the sql
/// </summary>
SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(_sql);
}

/// <summary>
/// initilizing the sql file
/// </summary>
void SqliteDatabase::initDB()
{
	std::string creation_code = "BEGIN;\nCREATE TABLE User (username TEXT PRIMARY KEY NOT NULL , password TEXT NOT NULL , email TEXT NOT NULL, addres TEXT NOT NULL, phone TEXT NOT NULL, birth_date TEXT NOT NULL);\nCREATE TABLE Questions (question TEXT PRIMARY KEY NOT NULL , correct TEXT NOT NULL , wrong1 TEXT NOT NULL, wrong2 TEXT NOT NULL, wrong3 TEXT NOT NULL);\nCREATE TABLE Game (game_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , players TEXT NOT NULL , game_date TEXT NOT NULL);\nCREATE TABLE Statistics (statistics_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , game_id INTEGER NOT NULL , username TEXt NOT NULL, correct_answers INTEGER NOT NULL, answers INTEGER NOT NULL, avg_answer_time REAL NOT NULL, game_score REAL NOT NULL, FOREIGN KEY(game_id) REFERENCES Game(game_id) ON DELETE CASCADE, FOREIGN KEY(username) REFERENCES User(username) ON DELETE CASCADE);\nCOMMIT;\n";
	char* err_m = { 0 };

	int res = sqlite3_exec(_sql, creation_code.c_str(), nullptr, nullptr, &err_m);
	if (res != SQLITE_OK) {
		std::cout << err_m;
		exit(1);
	}
}

/// <summary>
/// get GameSummary struct that contains the id of the game, the name of the player that the summery is 
/// about, and his finel score. the function complite the empty fields of the summery (placment and date)
/// </summary>
/// <param name="half_finished_gs"></param>
void SqliteDatabase::buildGameSummary(GameSummary& half_finished_gs)
{
	std::pair<std::string, std::string> info;

	//getting the rest if the information needed to build the summary
	std::string code = "SELECT game_date, players FROM Game WHERE game_id == " + 
		std::to_string(half_finished_gs.game_id) + ";";

	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), dateAndPlayersCallback, &info, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	int placment = 0;
	int players_count = 0;

	while (info.second.find(USER_NAMES_SEPARATOR) != std::string::npos) {
		players_count++;

		//checking it the first user name in the list is our user name
		if (info.second.substr(0, info.second.find(USER_NAMES_SEPARATOR)) == half_finished_gs.user_name)
			placment = players_count;

		//removing the user name that was checked
		info.second = info.second.substr(info.second.find(USER_NAMES_SEPARATOR) + 1);
	}

	//taking care of the last user name (the loop works until there are no mor separators, wich means the if the string look 
	//like that: "1|2|3|4", the loop will check 1, 2 and 3 but not 4. so we check it here)
	players_count++;
	if (!placment) { placment = players_count; }


	//building the summary
	half_finished_gs.placment = std::to_string(placment) + "/" + std::to_string(players_count);
	half_finished_gs.date = info.first;
}

/// <summary>
/// get the id of game that have the same creation date as the input date
/// </summary>
/// <param name="game_date"></param>
/// <returns></returns>
int SqliteDatabase::getGameId(std::string game_date)
{
	int res = 0;
	std::string code = "SELECT game_id FROM Game WHERE game_date == \'" + game_date + "\' LIMIT 1;";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), intCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return res;
}

/// <summary>
/// checking if user exist in the db
/// </summary>
/// <param name="user_name"> the name of the user to search </param>
/// <returns></returns>
bool SqliteDatabase::doesUserExist(const std::string& user_name)
{
	int res = 0;
	std::string code = "SELECT username FROM User WHERE username == \'" + user_name + "\' LIMIT 1;";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), checkIfSomeThingReturnedCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }
	if (res == SQL_ERROR_CODE) { throw std::exception("sqlError"); }

	return res;
}

/// <summary>
/// checking if there is a user that have the input password
/// </summary>
/// <param name="user_name"> the name of the user to check </param>
/// <param name="password"> the password to check </param>
/// <returns></returns>
bool SqliteDatabase::doesPasswordMatch(const std::string& user_name, const std::string& password)
{
	int res = 0;
	std::string code = "SELECT username FROM User WHERE username == \'" + user_name + "\' AND password == \'" + 
		password + "\'LIMIT 1;";

	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), checkIfSomeThingReturnedCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }
	if (res == SQL_ERROR_CODE) { throw std::exception("sqlError"); }

	return res;
}

/// <summary>
/// adding new user to the db
/// </summary>
/// the data of the new user 
/// <param name="user_name"></param>
/// <param name="password"></param>
/// <param name="email"></param>
void SqliteDatabase::addNewUser(const std::string& user_name, const std::string& password, const std::string& email, const std::string& addres, const std::string& phone, const std::string& birth_date)
{
	std::string code = "INSERT INTO User (username, password, email, addres, phone, birth_date) VALUES(\'" + 
		user_name + "\', \'" + password + "\', \'" + email + "\', \'" + addres + "\', \'" + phone + "\', \'" + 
		birth_date + "\');\n";

	char* err_m = { 0 };

	int res = sqlite3_exec(_sql, code.c_str(), nullptr, nullptr, &err_m);
	if (res != SQLITE_OK) { throw std::exception(err_m); }
}

float SqliteDatabase::getPlayerAverageAnswerTime(const std::string& user_name)
{
	float stats = 0;
	std::string code = "SELECT avg(avg_answer_time) FROM Statistics WHERE username == \'" + user_name + "\';";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), floatCallback, &stats, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return stats;
}

int SqliteDatabase::getNumOfCorrectAnswers(const std::string& user_name)
{
	int res = 0;
	std::string code = "SELECT sum(correct_answers) FROM Statistics WHERE username == \'" + user_name + "\';";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), intCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return res;
}

std::vector<Question> SqliteDatabase::getQuestions(int number_of_questions)
{
	std::vector<Question> res;
	std::string code = "SELECT * FROM Questions ORDER BY RANDOM() LIMIT " + std::to_string(number_of_questions) + ";";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), questionsCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return res;
}

void SqliteDatabase::insertNewQuestion(AddQuestionRequest q)
{
	std::string code = "INSERT INTO Questions (question, correct, wrong1, wrong2, wrong3) VALUES(\'" + 
		q.question + "\', \'" + q.answers[0] + "\', \'" + q.answers[1] + "\', \'" 
		+ q.answers[2] + "\', \'" + q.answers[3] + "\');\n";

	char* err_m = { 0 };

	int res = sqlite3_exec(_sql, code.c_str(), nullptr, nullptr, &err_m);
	if (res != SQLITE_OK) { throw std::exception(err_m); }
}

int SqliteDatabase::getNumOfTotalAnswers(const std::string& user_name)
{
	int res = 0;
	std::string code = "SELECT sum(answers) FROM Statistics WHERE username == \'" + user_name + "\';";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), intCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return res;
}

int SqliteDatabase::getNumOfPlayerGames(const std::string& user_name)
{
	int res = 0;
	std::string code = "SELECT count(*) FROM Statistics WHERE username == \'" + user_name + "\';";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), intCallback, &res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	return res;
}

/// <summary>
/// get the last five games of the user that have the input username
/// </summary>
/// <param name="user_name"></param>
/// <returns></returns>
std::vector<GameSummary> SqliteDatabase::getLastFiveGames(const std::string& user_name)
{
	std::vector<GameSummary> db_res;

	//getting the five game of the user sorted by the statistics id (the bigger the id is, the latest the statistics are)
	std::string code = "SELECT game_id, game_score FROM Statistics WHERE username == \'" + user_name + 
		"\' ORDER BY statistics_id DESC LIMIT 5;";

	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), gameIdAndScoreCallback, &db_res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	//buliding sammery for each game
	for (int i = 0; i < db_res.size(); i++) {
		db_res[i].user_name = user_name;
		buildGameSummary(db_res[i]);
	}

	return db_res;
}

std::vector<GameSummary> SqliteDatabase::getBestFiveGames()
{
	std::vector<GameSummary> db_res;

	std::string code = "SELECT game_id, game_score, username FROM Statistics ORDER BY game_score DESC LIMIT 5;";
	char* err_m = { 0 };

	int res1 = sqlite3_exec(_sql, code.c_str(), gameIdAndScoreCallback, &db_res, &err_m);
	if (res1 != SQLITE_OK) { throw std::exception(err_m); }

	//buliding sammery for each game
	for (int i = 0; i < db_res.size(); i++)
		buildGameSummary(db_res[i]);

	return db_res;
}

void SqliteDatabase::insertNewStatistics(GameData game_data, std::string user_name, unsigned int game_id, unsigned int number_of_q, float q_time)
{
	//the calculation of the finel score of the user is the precent of the correct answers
	//from all the answers + the precent of spare time he had from the questions (q answer time - avg answer time) 
	float game_score = ((1 - (game_data.averangeAnswerTime / q_time)) * FULL_PRESENT) + ((game_data.correctAnswerCount / (float)number_of_q) * FULL_PRESENT);

	std::string code = "INSERT INTO Statistics (game_id, username, correct_answers, answers, avg_answer_time, game_score) VALUES(" +
		std::to_string(game_id) + ", \'" + user_name + "\', " + std::to_string(game_data.correctAnswerCount)
		+ ", " + std::to_string(game_data.correctAnswerCount + game_data.wrongAnswerCount) + ", " +
		std::to_string(game_data.averangeAnswerTime) + ", " + std::to_string(game_score) + ");\n";

	char* err_m = { 0 };

	int res = sqlite3_exec(_sql, code.c_str(), nullptr, nullptr, &err_m);
	if (res != SQLITE_OK) { throw std::exception(err_m); }
}

int SqliteDatabase::insertNewGame(std::vector<std::string> users)
{
	std::string players, date;
	for (std::string user_name : users)
		players += USER_NAMES_SEPARATOR + user_name;
	players = players.substr(SECOND_CHAR);

	date = getCurrentTimeAsString();
	std::string code = "INSERT INTO Game (players, game_date) VALUES(\'" + players + "\', \'" + date + "\');\n";
	char* err_m = { 0 };

	int res = sqlite3_exec(_sql, code.c_str(), nullptr, nullptr, &err_m);
	if (res != SQLITE_OK) { throw std::exception(err_m); }
	return getGameId(date);
}
