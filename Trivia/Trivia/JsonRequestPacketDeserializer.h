#pragma once

#include "Request.h"
#include "json.hpp"
using json = nlohmann::json;


class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(const std::string& buffer);
	static SignupRequest deserializeSignupRequest(const std::string& buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const std::string& buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const std::string& buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(const std::string& buffer);
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(const std::string& buffer);
	static AddQuestionRequest deserializerAddQuestionRequest(const std::string& buffer);
};