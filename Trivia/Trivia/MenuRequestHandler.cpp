#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "SqliteDatabase.h"

//class SqliteDatabase;

MenuRequestHandler::MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticManager, RequestHandlerFactory& handlerFactory, LoggedUser user) :
	m_handlerFactory(handlerFactory),
	m_statisticsManager(statisticManager),
	m_roomManager(roomManager)
{
	m_user = user;
}

bool MenuRequestHandler::isRequestRelevant(const RequestInfo& info)
{
	return info.requestId == CREATE_ROOM_CODE ||
		info.requestId == GET_ROOMS_CODE ||
		info.requestId == GET_PLAYERS_IN_ROOM_CODE ||
		info.requestId == JOIN_ROOM_CODE ||
		info.requestId == GET_PERSONAL_STATS_CODE ||
		info.requestId == LOGOUT_CODE ||
		info.requestId == GET_HIGH_SCORE_CODE ||
		info.requestId == ADD_QUESTION_CODE ||
		info.requestId == GET_LAST_GAMES_CODE;
}

RequestResult MenuRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result;

	switch (info.requestId)
	{
	case CREATE_ROOM_CODE:
		result = createRoom(info);
		break;

	case GET_ROOMS_CODE:
		result = getRooms(info);
		break;

	case GET_PLAYERS_IN_ROOM_CODE:
		result = getPlayersInRoom(info);
		break;

	case JOIN_ROOM_CODE:
		result = joinRoom(info);
		break;

	case GET_PERSONAL_STATS_CODE:
		result = getPersonalStats(info);
		break; 

	case LOGOUT_CODE:
		result = signout(info);
		break; 

	case GET_HIGH_SCORE_CODE:
		result = getHighScore(info);
		break; 

	case GET_LAST_GAMES_CODE:
		result = getLastGames(info);
		break;

	case ADD_QUESTION_CODE:
		result = addQuestion(info);
		break;

	default:
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
		result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ UNRELEVNT_MSG }));
		break;
	}
	return result;
}

RequestResult MenuRequestHandler::signout(const RequestInfo& info)
{
	RequestResult result;

	if (m_handlerFactory.getLoginManager().logout(m_user.m_username))
	{
		result.buffer = JsonResponsePacketSerializer::serializeResponse(LogoutResponse({ SUCCESS_CODE }));
		result.newHandler = m_handlerFactory.createLoginRequestHandler();
	}
	else
	{
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
		result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ LOGOUT_ERROR }));
	}

	return result;
}

RequestResult MenuRequestHandler::getRooms(const RequestInfo& info)
{
	RequestResult result;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse({ SUCCESS_CODE , m_roomManager.getRooms() }));
	
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(const RequestInfo& info)
{
	RequestResult result;

	GetPlayersInRoomRequest playersRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(info.buffer);
	
	std::vector<Room*> rooms = m_roomManager.getRooms();
	
	for (Room* room : rooms)
	{
		//finding the room
		if (room->getRoomData()->id == playersRequest.roomId)
		{
			result.buffer = JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse({ SUCCESS_CODE , room->getAllUsers() }));
			result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

			return result;
		}
	}
	result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ FIND_ROOM_ERROR }));
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult MenuRequestHandler::getPersonalStats(const RequestInfo& info)
{
	RequestResult result;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(getPersonalStatsResponse({ SUCCESS_CODE , m_statisticsManager.getUserStatistics(m_user.m_username) }));

	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult MenuRequestHandler::getHighScore(const RequestInfo& info)
{
	RequestResult result;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse({ SUCCESS_CODE , m_statisticsManager.getHighScore() }));

	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult MenuRequestHandler::joinRoom(const RequestInfo& info)
{
	RequestResult result;

	JoinRoomRequest joinInfo = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);

	std::vector<Room*> rooms = m_roomManager.getRooms();

	for (Room* room : rooms)
	{
		if (room->getRoomData()->id == joinInfo.roomId)
		{
			//checking if there is the room is full
			if (!room->addUser(m_user)) {
				result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ ROOM_FULL_ERROR }));
				result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
			}
			//checking if the game have already started
			else if (room->getRoomData()->isActive)
			{
				result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ ROOM_ACTIVE_ERROR }));
				result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
			}
			else {
				result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, joinInfo.roomId);
				result.buffer = JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse({ SUCCESS_CODE }));
			}
			return result;
		}
	}

	result.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse({ FIND_ROOM_ERROR }));
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	
	return result;
}

RequestResult MenuRequestHandler::createRoom(const RequestInfo& info)
{
	RequestResult result;
	RoomData room_data;

	CreateRoomRequest roomDetails = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);

	room_data.isActive = WAIT_TO_START_THE_GAME;
	room_data.maxPlayers = roomDetails.maxUsers;
	room_data.name = roomDetails.roomName;
	room_data.numOfQuestionsInGame = roomDetails.questionCount;
	room_data.timePerQuestion = roomDetails.answerTimeout;



	m_roomManager.createRoom(m_user, room_data);
	
	result.buffer = JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse({SUCCESS_CODE}));
	result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, room_data.id);
	return result;
}

RequestResult MenuRequestHandler::getLastGames(const RequestInfo& info)
{
	RequestResult result;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(getLastGamesOfPlayer({ m_statisticsManager.getUsersLastGames(m_user.m_username), SUCCESS_CODE }));

	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}

RequestResult MenuRequestHandler::addQuestion(const RequestInfo& info)
{
	RequestResult result;

	AddQuestionRequest q = JsonRequestPacketDeserializer::deserializerAddQuestionRequest(info.buffer);
	SqliteDatabase::getInstance().insertNewQuestion(q);

	result.buffer = JsonResponsePacketSerializer::serializeResponse(AddQuestionResponse({ SUCCESS_CODE }));
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	return result;
}
