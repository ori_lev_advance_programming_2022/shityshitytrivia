#pragma once

#include "LoginManager.h"
#include <string>
#include <vector>

struct LoggedUser;
class Game;

enum {
	WAIT_TO_START_THE_GAME = 0,
	ACTIVE
};


struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};


class Room {
public:
	Room() = default;
	Room(RoomData data);
	bool addUser(const LoggedUser& user);
	void removeUser(const LoggedUser& user);
	std::vector<std::string> getAllUsers() const;
	RoomData* getRoomData();


private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;
};