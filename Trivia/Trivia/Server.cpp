#include "Server.h"
#include "SqliteDatabase.h"

#include <iostream>
#include <thread>

#define EXIT_SIGNAL "EXIT"



Server::Server() : m_communicator(Communicator::getInstance(m_handlerFactory)), m_handlerFactory(RequestHandlerFactory::getInstance(m_database)), m_database(&SqliteDatabase::getInstance()) {}

Server& Server::getInstance()
{
	static Server instance;
	return instance;
}


void Server::run()
{
	std::string exit_signal;
	std::thread communicator_thread(&Communicator::startHandleRequests, std::ref(m_communicator));
	communicator_thread.detach();

	//getting data from the user in loop untill he ask to close the server
	std::cout << "\n\n|||| Type EXIT to stop the server ||||\n\n";
	do
	{
		std::cin >> exit_signal;
	} while (exit_signal != EXIT_SIGNAL);
}
