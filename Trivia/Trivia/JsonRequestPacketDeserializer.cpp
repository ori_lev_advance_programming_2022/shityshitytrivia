#include "JsonRequestPacketDeserializer.h"

// parse the buffer to loginRequest struct
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["username"], j["password"] };
}

// parse the buffer to SignupRequest struct
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const std::string& buffer)
{
	json j = json::parse(buffer);
	
	return { j["Username"], j["Password"], j["Email"], j["addres"], j["phone"], j["birth_date"]};
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["roomId"] };
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["roomId"] };
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["roomName"], j["maxUsers"], j["questionCount"], j["answerTimeout"]};
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["answerId"], j["answerTime"]};
}

AddQuestionRequest JsonRequestPacketDeserializer::deserializerAddQuestionRequest(const std::string& buffer)
{
	json j = json::parse(buffer);

	return { j["question"], j["answers"]};
}
