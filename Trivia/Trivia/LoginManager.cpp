#pragma once 
#include <exception>
#include <regex>

#include "LoginManager.h"
#include "SqliteDatabase.h"
#include "RoomManager.h"

#define INVALID_PASSWORD "The password fromat is invalid. The password need to be 8 characters that contains at list one Apper letter, one Low letter, and at list one of the special (*&^%$#@!)."
#define INVALID_ADDRES "The addres format is invalid. The addres format need to look like that: '<street(apper and low case letters only)>, <apt(only numbers)>, <city(apper and low case letters only)>'"
#define INVALID_BIRTH_DATE "The birth date format is invalid. The format need to look like that: 'DD/MM/YYYY' OR 'DD.MM.YYYY'"
#define INVALID_PHONE_NAMBER "The phone number format is invalid. its need to look like that: '0_-________' or '0__-_______'"
#define INVALID_EMAIL "The email format is invalid. The email format is the same format of emails in any website or program"

#define USER_ALREADY_EXIST_ERROR "There is allready a user with this user name. Try something else"
#define USER_NOT_EXIST_ERROR "There is no user with this user name"
#define USER_ALREADY_CONNECTED "This user is allready connected"

LoginManager::LoginManager(IDatabase* db)
{
	m_database = db;
}

LoginManager& LoginManager::getInstance(IDatabase* db)
{
	static LoginManager instance(db);
	return instance;
}

std::string LoginManager::signup(const SignupRequest& info)
{
	//checking if there is allready user with that user name
	if (m_database->doesUserExist(info.username))
		return USER_ALREADY_EXIST_ERROR;

	//checking if the user info is matching the instarctions (with regex) 
	if (checkSignupRequest(info).size())
		return checkSignupRequest(info);

	try
	{
		m_database->addNewUser(info.username, info.password, info.email, info.addres, info.phone, info.birth_date);
	}
	catch (const std::exception&) { return SIGNUP_ERROR; }

	m_loggedUsers.push_back(LoggedUser({ info.username }));

	return "";
}

std::string LoginManager::login(const LoginRequest& info)
{
	//checking if the user exist
	if (!m_database->doesUserExist(info.username))
		return USER_NOT_EXIST_ERROR;

	//checking if the user have already conected 
	for(LoggedUser user : m_loggedUsers)
		if (user.m_username == info.username)
			return USER_ALREADY_CONNECTED;

	try
	{
		if (m_database->doesPasswordMatch(info.username, info.password)) {
			std::lock_guard<std::mutex> lock(m_locker);
			m_loggedUsers.push_back({ info.username });
			return "";
		}
	}
	catch (const std::exception&) { ; }

	return LOGIN_ERROR;
}

bool LoginManager::logout(const std::string& user_name)
{
	//go through all the logged user and find the searched user, then erase him
	for (std::vector<LoggedUser>::iterator user = m_loggedUsers.begin(); user != m_loggedUsers.end(); user++) {
		if (user->m_username == user_name) {
			std::lock_guard<std::mutex> lock(m_locker);
			m_loggedUsers.erase(user);

			//deleting the user from his room's users list in case he was in a room
			std::vector<Room*> rooms = RoomManager::getInstance().getRooms();
			for (Room* room : rooms)
				for (std::string user : room->getAllUsers())
					if (user == user_name)
						room->removeUser(LoggedUser{ user_name });

			return true;
		}
	}
	
	return false;
}

std::string LoginManager::checkSignupRequest(const SignupRequest& request)
{
	//check the email
	if(!(std::regex_match(request.email, std::regex("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"))))
		return INVALID_EMAIL;

	//check the password
	if (!(std::regex_match(request.password, std::regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[*&^%$#@!]).{8}$"))))
		return INVALID_PASSWORD;

	//check the addres
	if (!(std::regex_match(request.addres, std::regex("^[A-Za-z]+, [0-9]+, [A-Za-z]+$"))))
		return INVALID_ADDRES;

	//check the phone number
	if (!(std::regex_match(request.phone, std::regex("^(0[1-9]{1,2}[-][0-9]{6,14})$"))))
		return INVALID_PHONE_NAMBER;

	//check the birth date
	if (!(std::regex_match(request.birth_date, std::regex("^([0-9]{2}[/.][0-9]{2}[/.][0-9]{4})$"))))
		return INVALID_BIRTH_DATE;

	return std::string("");
}
