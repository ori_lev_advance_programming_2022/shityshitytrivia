#pragma once
#include <iostream>
#include <vector>
#include <mutex>
#include "IDatabase.h"
#include "Request.h"

class Game;
class IDatabase;

struct LoggedUser
{
	std::string m_username;
};

class LoginManager
{
public:
	static LoginManager& getInstance(IDatabase* db);

	LoginManager(const LoginManager&) = delete;
	void operator=(const LoginManager&) = delete;

	std::string signup(const SignupRequest& info);
	std::string login(const LoginRequest& info);
	bool logout(const std::string& user_name);

private: 
	std::string checkSignupRequest(const SignupRequest& request);
	LoginManager(IDatabase* db);

	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;

	std::mutex m_locker;
};

