#pragma once
#include "IDatabase.h"
#include "sqlite3.h"

class SqliteDatabase : public IDatabase
{
public:
	static SqliteDatabase& getInstance();
	~SqliteDatabase();

	SqliteDatabase(const SqliteDatabase&) = delete;
	void operator=(const SqliteDatabase&) = delete;

	virtual bool doesUserExist(const std::string& user_name) override;
	virtual bool doesPasswordMatch(const std::string& user_name, const std::string& password) override;
	virtual void addNewUser(const std::string& user_name, const std::string& password, const std::string& email, const std::string& addres, const std::string& phone, const std::string& birth_date) override;
	
	virtual std::vector<Question> getQuestions(int number_of_questions) override;
	virtual void insertNewQuestion(AddQuestionRequest q) override;

	virtual float getPlayerAverageAnswerTime(const std::string& user_name) override;
	virtual int getNumOfCorrectAnswers(const std::string& user_name) override;
	virtual int getNumOfTotalAnswers(const std::string& user_name) override;
	virtual int getNumOfPlayerGames(const std::string& user_name) override;

	virtual std::vector<GameSummary> getLastFiveGames(const std::string& user_name) override;
	virtual std::vector<GameSummary> getBestFiveGames() override;

	virtual void insertNewStatistics(GameData game_data, std::string user_name, unsigned int game_id, unsigned int number_of_q, float q_time) override;
	virtual int insertNewGame(std::vector<std::string> users) override;

private:
	SqliteDatabase();
	
	void initDB();
	void buildGameSummary(GameSummary& half_finished_gs);
	int getGameId(std::string game_date);

	sqlite3* _sql;
};

