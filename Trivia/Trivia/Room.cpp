#include "Room.h"

#include <algorithm>

Room::Room(RoomData data) : m_metadata(data)
{}

bool Room::addUser(const LoggedUser& user)
{
	if (getAllUsers().size() < getRoomData()->maxPlayers) {
		m_users.push_back(user);
		return true;
	}
	else
		return false;
}

void Room::removeUser(const LoggedUser& user)
{
	for (int i = 0; i < m_users.size(); i++)
		if (m_users[i].m_username == user.m_username) {
			m_users.erase(m_users.begin() + i);
			break;
		}
}

std::vector<std::string> Room::getAllUsers() const
{
	std::vector<std::string> users;
	for (const auto& user : m_users)
		users.emplace_back(user.m_username);
	
	return users;
}

RoomData* Room::getRoomData()
{
	return &m_metadata;
}
