#pragma once
#include <string>
#include <vector>

enum CODES {
	ERROR_CODE = 0,
	SUCCESS_CODE,
	LOGIN_CODE,
	SIGNUP_CODE,
	LOGOUT_CODE,
	CREATE_ROOM_CODE,
	ADD_QUESTION_CODE,
	JOIN_ROOM_CODE,
	GET_ROOMS_CODE,
	GET_PLAYERS_IN_ROOM_CODE,
	GET_HIGH_SCORE_CODE,
	GET_LAST_GAMES_CODE,
	GET_PERSONAL_STATS_CODE,
	GET_ROOM_STATE_CODE,
	LEAVE_ROOM_CODE,
	CLOSE_ROOM_CODE,
	START_GAME_CODE,
	LEAVE_GAME_CODE,
	GET_QUESTIONS_CODE,
	SUBMIT_ANSWER_CODE,
	GET_GAME_RESULTS_CODE
};

#define LOGIN_ERROR "ERROR: Can't loggin"
#define SIGNUP_ERROR "ERROR: Can't signup"
#define UNRELEVNT_MSG "ERROR: Request is not relevent. try again you dockhead"
#define FIND_ROOM_ERROR "ERROR: Room does not exist"
#define ROOM_FULL_ERROR "ERROR: The room contains the max number of players he can contain"
#define ROOM_ACTIVE_ERROR "ERROR: The game have allready stated. try next time."
#define LOGOUT_ERROR "ERROR: Can't log out"

struct LoginRequest
{
	std::string username;
	std::string password;
};


struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
	std::string addres;
	std::string phone;
	std::string birth_date;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};



struct JoinRoomRequest
{
	unsigned int roomId;
};


struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};


struct SubmitAnswerRequest
{
	unsigned int answerId;
	float answerTime;
};

struct AddQuestionRequest
{
	std::string question;
	std::vector<std::string> answers;
};
