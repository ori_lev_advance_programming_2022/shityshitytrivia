#pragma once

#include "Room.h"

#include <map>
#include <mutex>
class RoomManager {
public:
	static RoomManager& getInstance();

	RoomManager(const RoomManager&) = delete;
	void operator=(const RoomManager&) = delete;

	void createRoom(LoggedUser& owner, RoomData& data);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<Room*> getRooms();

private:
	RoomManager();

	unsigned int last_room_id;
	std::map<int, Room> m_rooms;
	std::mutex m_locker;
};