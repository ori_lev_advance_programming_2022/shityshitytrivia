﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Page
    {
        public Signup()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SignUpRequest info = new SignUpRequest();
            info.Username = UsernameTextBox.Text;
            info.Password = PasswordTextBox.Text;
            info.phone = PhoneTextBox.Text;
            info.birth_date = BirthTextBox.Text;
            info.addres = LocationTextBox.Text;
            info.Email = MailTextBox.Text;

            if (info.Username == "" || info.phone == "" || info.Password == "" || info.Email == ""
                || info.birth_date == "" || info.addres == "")
                ErrorLabel.Content = "ERROR: some of the field are empty";

            string res = Communicator.getInstnace().Signup(info);
            if (res.Length == 0)
            {
                NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
            }
            ErrorLabel.Content = res;
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Login.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
