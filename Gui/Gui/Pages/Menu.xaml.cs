﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Statistics.xaml", UriKind.RelativeOrAbsolute));
        }

        private void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/CreateRoom.xaml", UriKind.RelativeOrAbsolute));
        }

        private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/JoinRoom.xaml", UriKind.RelativeOrAbsolute));
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (Communicator.getInstnace().Logout())
                NavigationService.Navigate(new Uri("/Pages/Login.xaml", UriKind.RelativeOrAbsolute));            
        }

        private void AddQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/AddQuestion.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
