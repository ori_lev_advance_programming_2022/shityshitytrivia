﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Page
    {
        public Statistics()
        {
            InitializeComponent();

            
            StatisticsData data = Communicator.getInstnace().GetStatistics();

            AverageAnswerTimeLabel.Content += data.stats.average_answer_time.ToString();
            CurrentAnswerRateLabel.Content += data.stats.correct_answers_rate.ToString();
            TotalOfAnswersLabel.Content += data.stats.total_of_answers.ToString();
            TotalOfCurrentAnswersLabel.Content += data.stats.total_of_correct_answers.ToString();
            TotalOfGamesLabel.Content += data.stats.total_of_games.ToString();

            int counter = 0;
            foreach(var item in data.last_games)
            {
                LastGamesGrid.RowDefinitions.Add(new RowDefinition());

                counter++;

                string[] rowData = new string[] { 
                    counter.ToString() + ".",
                    item.game_id.ToString(),
                    item.date,
                    item.finel_score.ToString(),
                    item.placment.ToString() 
                };


                for (int j = 0; j < rowData.Length; j++)
                {
                    Label val = new Label() { Content = rowData[j] };
                    LastGamesGrid.Children.Add(val);
                    val.SetValue(Grid.RowProperty, LastGamesGrid.RowDefinitions.Count - 1);
                    val.SetValue(Grid.ColumnProperty, j);
                }
            }

            counter = 0;
            foreach (var item in data.best_games)
            {
                HighScoreGrid.RowDefinitions.Add(new RowDefinition());

                counter++;

                string[] rowData = new string[] {
                    counter.ToString() + ".",
                    item.game_id.ToString(),
                    item.user_name, 
                    item.date,
                    item.finel_score.ToString(),
                    item.placment.ToString()
                };

                for (int j = 0; j < rowData.Length; j++)
                {
                    Label val = new Label() { Content = rowData[j] };
                    HighScoreGrid.Children.Add(val);
                    val.SetValue(Grid.RowProperty, HighScoreGrid.RowDefinitions.Count - 1);
                    val.SetValue(Grid.ColumnProperty, j);
                }
            }

        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
