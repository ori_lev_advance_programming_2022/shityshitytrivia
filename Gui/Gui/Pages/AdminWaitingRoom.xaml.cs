﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for AdminWaitingRoom.xaml
    /// </summary>
    public partial class AdminWaitingRoom : Page
    {
        private BackgroundWorker worker;
        

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //sins we are not planing on stop apdating the screen as long as the user wants to,
            //the progras precent will be 50% allways and the loop will be endless
            for (int i = 0; i < 1000; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                
                RoomsState room_s = Communicator.getInstnace().GetRoomState();
                worker.ReportProgress(i, room_s.players);

                Thread.Sleep(3000);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] players = e.UserState as string[];

            PlayersGrid.Children.Clear();
            for (int i = PlayersGrid.RowDefinitions.Count - 1; i > 0; i--)
                PlayersGrid.RowDefinitions.RemoveAt(i);

            if (players != null && players.Length > 1)
                foreach (string user_name in players)
                {
                    if (user_name != Communicator.getInstnace().m_userName) 
                    {
                        PlayersGrid.RowDefinitions.Add(new RowDefinition());

                        Label name = new Label() { Content = user_name };
                        name.HorizontalContentAlignment = HorizontalAlignment.Center;
                        PlayersGrid.Children.Add(name);
                        name.SetValue(Grid.RowProperty, PlayersGrid.RowDefinitions.Count - 1);
                        name.SetValue(Grid.ColumnProperty, 1);
                    }
                }
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //nothing bla bla bla
            int i = 5 + 6;
            //just caus' i can hahaha
        }
        public AdminWaitingRoom()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;

            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void CloseRoomButton_Click(object sender, RoutedEventArgs e)
        {
            if(Communicator.getInstnace().CloseRoom())
            {
                worker.CancelAsync();
                NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
            }
        }

        private void StartGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (Communicator.getInstnace().StartGame())
            {
                worker.CancelAsync();
                NavigationService.Navigate(new Uri("/Pages/Game.xaml", UriKind.RelativeOrAbsolute));
            }
        }
    }
}
