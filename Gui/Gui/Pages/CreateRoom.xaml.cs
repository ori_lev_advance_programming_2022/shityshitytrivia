﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Page
    {
        public CreateRoom()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            bool flag = true;
            CreateRoomRequest info = new CreateRoomRequest();

            info.roomName = RoomNameTextBox.Text;

            try
            {
                info.answerTimeout = int.Parse(TimePerQuestionTextBox.Text);
                info.questionCount = int.Parse(QuestionCountTextBox.Text);
                info.maxUsers = int.Parse(MaxUsersTextBox.Text);
            } catch(Exception)
            {
                ErrorLabel.Content = "Error: Invalid data!";
                flag = false;
            }
            
            if(flag) {

                if (info.roomName == "")
                    ErrorLabel.Content = "ERROR: some of the field are empty";

                string res = Communicator.getInstnace().CreateRoom(info);

                if (res.Length == 0)
                    NavigationService.Navigate(new Uri("/Pages/AdminWaitingRoom.xaml", UriKind.RelativeOrAbsolute));
                else
                    ErrorLabel.Content = res;
            }
              
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
