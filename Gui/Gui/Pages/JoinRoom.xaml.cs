﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Page
    {
        private BackgroundWorker worker;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
           //sins we are not planing on stop apdating the screen as long as the user wants to,
           //the progras precent will be 50% allways and the loop will be endless
           while(true)
           {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                Dictionary<string, int> rooms = Communicator.getInstnace().GetRooms();
                worker.ReportProgress(50, rooms);
                Thread.Sleep(3000);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Dictionary<string, int> rooms = e.UserState as Dictionary<string, int>;

            //deleting the current data in the grid
            RoomsGrid.Children.Clear();
            for (int i = RoomsGrid.RowDefinitions.Count - 1; i > 0; i--)
                RoomsGrid.RowDefinitions.RemoveAt(i);

            if (rooms != null)
                foreach (KeyValuePair<string, int> kvp in rooms)
                {
                    RoomsGrid.RowDefinitions.Add(new RowDefinition());

                    Label roomId = new Label() { Content = kvp.Value };
                    RoomsGrid.Children.Add(roomId);
                    roomId.SetValue(Grid.RowProperty, RoomsGrid.RowDefinitions.Count - 1);

                    Label roomName = new Label() { Content = kvp.Key };
                    RoomsGrid.Children.Add(roomName);
                    roomName.SetValue(Grid.RowProperty, RoomsGrid.RowDefinitions.Count - 1);
                    roomName.SetValue(Grid.ColumnProperty, 1);
                }
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //nothing bla bla bla
            int i = 5 + 6;
            //just caus' i can hahaha
        }
        public JoinRoom()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;

            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }
        private void BackButton(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
        }

        private void JoinButton(object sender, RoutedEventArgs e)
        {
            try
            {
                string res = Communicator.getInstnace().JoinRoom(int.Parse(IdTextBox.Text));

                if (res.Length == 0)
                {
                    worker.CancelAsync();
                    NavigationService.Navigate(new Uri("/Pages/WaitingRoom.xaml", UriKind.RelativeOrAbsolute));
                }
                else
                    ErrorLabel.Content = res;
            }
            catch (Exception)
            {
                ErrorLabel.Content = "Error: Invalid data!";
            }
        }
    }
}
