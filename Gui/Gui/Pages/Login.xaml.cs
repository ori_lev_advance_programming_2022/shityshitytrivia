﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if ( PasswordTextBox.Password == "" || UsernameTextBox.Text == "")
                ErrorLabel.Content = "ERROR: some of the field are empty";

            string res = Gui.Communicator.getInstnace().Login(UsernameTextBox.Text, PasswordTextBox.Password);
            
            if (res.Length == 0)
            {
                NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
            }
            ErrorLabel.Content = res;
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Signup.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
