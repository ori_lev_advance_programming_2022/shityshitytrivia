﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for WaitingRoom.xaml
    /// </summary>
    public partial class WaitingRoom : Page
    {
        private BackgroundWorker worker;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //sins we are not planing on stop apdating the screen as long as the user wants to,
            //the progras precent will be 50% allways and the loop will be endless
            while (true)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                RoomsState room_s = Communicator.getInstnace().GetRoomState();

                if (room_s.status == (int)CODES.ERROR_CODE)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        string messageBoxText = "The room got closed by the room-admin";
                        string caption = "State update";
                        MessageBoxButton button = MessageBoxButton.OK;
                        MessageBoxImage icon = MessageBoxImage.Information;
                        MessageBox.Show(messageBoxText, caption, button, icon, MessageBoxResult.Yes);

                        worker.CancelAsync();

                        NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else if (room_s.has_game_begun)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        string messageBoxText = "The game started by the room-admin";
                        string caption = "State update";
                        MessageBoxButton button = MessageBoxButton.OK;
                        MessageBoxImage icon = MessageBoxImage.Information;
                        MessageBox.Show(messageBoxText, caption, button, icon, MessageBoxResult.Yes);

                        worker.CancelAsync();


                        NavigationService.Navigate(new Uri("/Pages/Game.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                    worker.ReportProgress(50, room_s.players);

                Thread.Sleep(3000);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] players = e.UserState as string[];

            PlayersGrid.Children.Clear();
            for (int i = PlayersGrid.RowDefinitions.Count - 1; i > 0; i--)
                PlayersGrid.RowDefinitions.RemoveAt(i);


            if (players.Length > 1)
                foreach (string user_name in players)
                {
                    if (user_name != Communicator.getInstnace().m_userName) { 
                        PlayersGrid.RowDefinitions.Add(new RowDefinition());

                        Label name = new Label() { Content = user_name };
                        name.HorizontalContentAlignment = HorizontalAlignment.Center;
                        PlayersGrid.Children.Add(name);
                        name.SetValue(Grid.RowProperty, PlayersGrid.RowDefinitions.Count - 1);
                    }
                }
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //nothing bla bla bla
            int i = 5 + 6;
            //just caus' i can hahaha
        }
        public WaitingRoom()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;

            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void LeaveRoomButton_Click(object sender, RoutedEventArgs e)
        {
            if (Communicator.getInstnace().LeaveRoom())
            {
                worker.CancelAsync();
                NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
            }
        }
    }
}
