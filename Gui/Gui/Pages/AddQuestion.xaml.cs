﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for AddQuestion.xaml
    /// </summary>
    public partial class AddQuestion : Page
    {
        public AddQuestion()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(correct.Text) && !String.IsNullOrEmpty(question.Text) && !String.IsNullOrEmpty(wrong1.Text) && !String.IsNullOrEmpty(wrong2.Text) && !String.IsNullOrEmpty(wrong3.Text))
            {
                AddQuestionR q = new AddQuestionR();

                q.question = question.Text;
                q.answers = new string[4] { correct.Text, wrong1.Text, wrong2.Text, wrong3.Text};

                Communicator.getInstnace().AddQuestion(q);
                NavigationService.Navigate(new Uri("/Pages/menu.xaml", UriKind.RelativeOrAbsolute));
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/menu.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
