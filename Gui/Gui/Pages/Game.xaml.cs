﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;




namespace Gui.Pages
{
    

    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Page
    {
        const int TIME_OUT = 9;
        const int CORRECT = 0;
        const int INCORRECT = 1;

        int correctIndex = 0;
        int counter = 0;
        int correctCounter = 0;
        RoomsState state;

        DispatcherTimer _timer;
        TimeSpan _time;
        

        public Game()
        {
            InitializeComponent();

            state = Communicator.getInstnace().GetLastRoomState();
            QuestionLeftLabel.Content = "Question Left: " + state.answer_count.ToString();
            RightQuestionLabel.Content = "Corrent Question: 0";
            counter = state.answer_count;

            generateNewQuestion();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Communicator.getInstnace().LeaveGame();
            NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
        }

        private void AnswerOne_Click(object sender, RoutedEventArgs e)
        {
            handleGame(0);
        }

        private void AnswerTwo_Click(object sender, RoutedEventArgs e)
        {
            handleGame(1);
        }

        private void AnswerThree_Click(object sender, RoutedEventArgs e)
        {
            handleGame(2);
        }

        private void AnswerFour_Click(object sender, RoutedEventArgs e)
        {
            handleGame(3);
        }


        private void Randomize<T>(T[] items)
        {
            Random rand = new Random();

            // For each spot in the array, pick
            // a random item to swap into that spot.
            for (int i = 0; i < items.Length - 1; i++)
            {
                int j = rand.Next(i, items.Length);
                T temp = items[i];
                items[i] = items[j];
                items[j] = temp;
            }
        }


        private void generateNewQuestion()
        {
            QuestionResponse response = Communicator.getInstnace().GetQuestion();

            if (response.answer == null) {
                if (_timer != null)
                    _timer.Stop();

                return;
            }
                string correctAnswer = response.answer[0];

            Randomize(response.answer);

            for(int i = 0; i < response.answer.Length; i++)
            {
                if(response.answer[i] == correctAnswer)
                {
                    correctIndex = i;
                    break;
                }
            }

            counter--;

            // put the question and the answers
            QuestionLabel.Content = response.question;

            Button[] buttons = new Button[] { AnswerOne, AnswerTwo, AnswerThree, AnswerFour };
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Background = Brushes.AliceBlue;
                buttons[i].Content = response.answer[i];
            }

            // put the timer
            _time = TimeSpan.FromSeconds(state.answer_timeout);

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                tbTime.Text = _time.ToString("c");
                if (_time == TimeSpan.Zero)
                {
                    _timer.Stop();
                    handleGame(TIME_OUT);
                }
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }

        private void handleGame(int index)
        {
            _timer.Stop();
            // submit the answer
            Answer ans = new Answer();
            ans.answerTime = Communicator.getInstnace().GetLastRoomState().answer_timeout - (float)_time.TotalSeconds;

            if (index == TIME_OUT)
                ans.answerId = TIME_OUT;
            else if (index == correctIndex)
            {
                ans.answerId = CORRECT;
                correctCounter++;
            }
            else
                ans.answerId = INCORRECT;

            Communicator.getInstnace().SubmitAnswer(ans);

            // change the colors to red/green
            Button[] buttons = new Button[] { AnswerOne, AnswerTwo, AnswerThree, AnswerFour };
            for (int i = 0; i < buttons.Length; i++)
            {
                if (i == correctIndex)
                {
                    buttons[i].Background = Brushes.LimeGreen;
                }
                else
                {
                    buttons[i].Background = Brushes.Red;
                }
            }

            // update data labels
            QuestionLeftLabel.Content = "Question Left: " + counter.ToString();
            RightQuestionLabel.Content = "Corrent Question: " + correctCounter.ToString();

            Thread.Sleep(1500);

            // if this is the last question, put him in the game result page
            if (counter == 0)
            {
               
                NavigationService.Navigate(new Uri("/Pages/GameResult.xaml", UriKind.RelativeOrAbsolute));
            }
            
            generateNewQuestion();
        }
    }
}
