﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace Gui.Pages
{
    /// <summary>
    /// Interaction logic for GameResult.xaml
    /// </summary>
    public partial class GameResult : Page
    {
        private BackgroundWorker worker;

        public void displayColls()
        {
            Label name = new Label() { Content = "Name" };
            ResGrid.Children.Add(name);
            name.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);

            Label correct_answers = new Label() { Content = "Current answers" };
            ResGrid.Children.Add(correct_answers);
            correct_answers.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
            correct_answers.SetValue(Grid.ColumnProperty, 1);

            Label avg_time = new Label() { Content = "Average Time Per Question" };
            ResGrid.Children.Add(avg_time);
            avg_time.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
            avg_time.SetValue(Grid.ColumnProperty, 2);

            Label score = new Label() { Content = "Total Score" };
            ResGrid.Children.Add(score);
            score.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
            score.SetValue(Grid.ColumnProperty, 3);
        }
        public float calculateScore(PlayerResults p)
        {
            float correct_rate = p.correctAnswerCount / (float)Communicator.getInstnace().GetLastRoomState().answer_count;
            float time_rate = p.averageAnswerCount / (float)Communicator.getInstnace().GetLastRoomState().answer_timeout;
            return ((1 - time_rate) + correct_rate) * 100;
        }
        public void displayWinning(PlayerResults[] res)
        {
            PlayerResults possible_winner = res[res.Length - 1];
            Array.Sort<PlayerResults>(res, (x, y) => (x.correctAnswerCount + x.wrongAnswerCount).CompareTo(y.correctAnswerCount + y.wrongAnswerCount));
            if (res[res.Length - 1].correctAnswerCount + res[res.Length - 1].wrongAnswerCount
                == Communicator.getInstnace().GetLastRoomState().answer_count)
            {
                Winner.Content = "The winner is: " + possible_winner.username + "!";
                if (Communicator.getInstnace().m_userName == possible_winner.username)
                    BackGroundGrid.Background = Brushes.GreenYellow;

                worker.CancelAsync();

                string messageBoxText = "The game ended";
                string caption = "Game update";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Information;
                MessageBox.Show(messageBoxText, caption, button, icon, MessageBoxResult.Yes);
            }

        }
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //sins we are not planing on stop apdating the screen as long as the user wants to,
            //the progras precent will be 50% allways and the loop will be endless
            while (true)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                GameResultsResponse res = Communicator.getInstnace().GetGameResults();
                worker.ReportProgress(50, res.results);
                Thread.Sleep(3000);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PlayerResults[] res = e.UserState as PlayerResults[];
            Array.Sort<PlayerResults>(res, (x, y) => calculateScore(x).CompareTo(calculateScore(y)));

            ResGrid.Children.Clear();
            for (int i = ResGrid.RowDefinitions.Count - 1; i > 0; i--)
                ResGrid.RowDefinitions.RemoveAt(i);

            displayColls();
            foreach (PlayerResults player in res)
            {
                ResGrid.RowDefinitions.Add(new RowDefinition());

                Label name = new Label() { Content = player.username };
                ResGrid.Children.Add(name);
                name.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);

                Label correct_answers = new Label() { Content = player.correctAnswerCount };
                ResGrid.Children.Add(correct_answers);
                correct_answers.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
                correct_answers.SetValue(Grid.ColumnProperty, 1);

                Label avg_time = new Label() { Content = player.averageAnswerCount };
                ResGrid.Children.Add(avg_time);
                avg_time.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
                avg_time.SetValue(Grid.ColumnProperty, 2);

                Label score = new Label() { Content = calculateScore(player).ToString("#.##") };
                ResGrid.Children.Add(score);
                score.SetValue(Grid.RowProperty, ResGrid.RowDefinitions.Count - 1);
                score.SetValue(Grid.ColumnProperty, 3);
            }

            displayWinning(res);
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //nothing bla bla bla
            int i = 5 + 6;
            //just caus' i can hahaha
        }
        public GameResult()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;

            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            worker.CancelAsync();
            Communicator.getInstnace().LeaveGame();
            NavigationService.Navigate(new Uri("/Pages/Menu.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
