﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Gui
{
    class Deserilaizer
    {
        public static ErrorResponse deserializeError(string Buffer)
        {
            return JsonConvert.DeserializeObject<ErrorResponse>(Buffer);
        }
        public static GetRoomResponse deserializeRooms(string Buffer)
        {
            return JsonConvert.DeserializeObject<GetRoomResponse>(Buffer);
        }
        public static GetPlayersInRoomResponse deserializePlayersInRoom(string Buffer)
        {
            return JsonConvert.DeserializeObject<GetPlayersInRoomResponse>(Buffer);
        }
        public static GetHighScoreResponse deserializeHighScore (string Buffer)
        {
            return JsonConvert.DeserializeObject<GetHighScoreResponse>(Buffer);
        }
        public static GetLastGamesResponse deserializeLastGames(string Buffer)
        {
            return JsonConvert.DeserializeObject<GetLastGamesResponse>(Buffer);
        }
        public static GetStatsResponse deserializeStats(string Buffer)
        {
            return JsonConvert.DeserializeObject<GetStatsResponse>(Buffer);
        }
        public static GetRoomStateResponse deserializeRoomState(string Buffer)
        {
            return JsonConvert.DeserializeObject<GetRoomStateResponse>(Buffer);
        }
        public static SubmitAnswerResponse deserializeSubmitAnswer(string Buffer)
        {
            return JsonConvert.DeserializeObject<SubmitAnswerResponse>(Buffer);
        }
        public static QuestionResponse deserializeQuestion(string Buffer)
        {
            return JsonConvert.DeserializeObject<QuestionResponse>(Buffer);
        }
        public static GameResultsResponse deserializeGameResults(string Buffer)
        {
            return JsonConvert.DeserializeObject<GameResultsResponse>(Buffer);
        }

    }
}
