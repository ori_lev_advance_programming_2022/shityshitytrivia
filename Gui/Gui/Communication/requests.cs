﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
	struct RequestInfo
	{
		public int requestId { get; set; }
		public string buffer { get; set; }
	};
	public class CreateRoomRequest
    {
        public string roomName { get; set; }
        public int maxUsers { get; set; }
		public int questionCount { get; set; }
		public int answerTimeout { get; set; }
	};

	enum CODES : int
	{
		ERROR_CODE = 0,
		SUCCESS_CODE,
		LOGIN_CODE,
		SIGNUP_CODE,
		LOGOUT_CODE,
		CREATE_ROOM_CODE,
		ADD_QUESTION_CODE,
		JOIN_ROOM_CODE,
		GET_ROOMS_CODE,
		GET_PLAYERS_IN_ROOM_CODE,
		GET_HIGH_SCORE_CODE,
		GET_LAST_GAMES_CODE,
		GET_PERSONAL_STATS_CODE,
		GET_ROOM_STATE_CODE,
		LEAVE_ROOM_CODE,
		CLOSE_ROOM_CODE,
		START_GAME_CODE,
		LEAVE_GAME_CODE,
		GET_QUESTIONS_CODE,
		SUBMIT_ANSWER_CODE,
		GET_GAME_RESULTS_CODE
	};

	
	public class LoginRequest
	{
		public string username { get; set; }
		public string password { get; set; }
	};

	public class SignUpRequest
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public string Email { get; set; }
		public string addres { get; set; }
		public string phone { get; set; }
		public string birth_date { get; set; }
	};

	public class GetPlayersInRoomRequest
	{
		public int roomId { get; set; }
	};

	public class JoinRoomRequest
	{
		public int roomId { get; set; }
	};

	public class Answer
	{
		public int answerId { get; set; }
		public float answerTime { get; set; }
    }

    public class AddQuestionR
    {
		public string question { get; set; }
		public string[] answers { get; set; }
	}

}
