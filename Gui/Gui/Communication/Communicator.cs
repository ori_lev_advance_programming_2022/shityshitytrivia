using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;



namespace Gui
{
    public class GameSummery
    {
        public float finel_score { get; set; }
        public string date { get; set; }
        public int game_id { get; set; }
        public string placment { get; set; }
        public string user_name { get; set; }
    }

    public class StatisticsData
    {
        public GameSummery[] last_games { get; set; }
        public GameSummery[] best_games { get; set; }
        public Stats stats { get; set; }
    }

    class Communicator
    {
        public string m_userName;
        public string m_roomName; 
        private TcpClient m_client;
        private static Communicator _instance;
        private RoomsState roomState;
        private static Mutex mutex = new Mutex();

        NetworkStream m_clientStream;

        public static Communicator getInstnace()
        {
            if(_instance == null)
                _instance = new Communicator();
            

            return _instance;
        }
        public string Login(string user_name, string password) 
        {
            LoginRequest login = new LoginRequest();
            login.username = user_name;
            login.password = password;

            RequestInfo res = Communicate(Serilaizer.SerilizeRequest(login));

            if(!(res.requestId == (int)CODES.ERROR_CODE)) {
                m_userName = user_name;
                return "";
            }

            return Deserilaizer.deserializeError(res.buffer).message;
        }
        public string Signup(SignUpRequest info) 
        {
            string buffer = Serilaizer.SerilizeRequest(info);
            RequestInfo res = Communicate(buffer);

            if (!(res.requestId == (int)CODES.ERROR_CODE))
            {
                m_userName = info.Username;
                return "";
            }

            return Deserilaizer.deserializeError(res.buffer).message;
        }
        public bool Logout()
        {
            string buffer = Serilaizer.SerilizeRequest((int)CODES.LOGOUT_CODE);
            RequestInfo res = Communicate(buffer);

            if (!(res.requestId == (int)CODES.ERROR_CODE))
            {
                m_userName = "";
                return true;
            }

            return false;
        }
        public string CreateRoom(CreateRoomRequest info) 
        {
            string buffer = Serilaizer.SerilizeRequest(info);
            RequestInfo res = Communicate(buffer);

            if (!(res.requestId == (int)CODES.ERROR_CODE))
            {
                m_roomName = info.roomName;
                return "";
            }

            return Deserilaizer.deserializeError(res.buffer).message;
        }
        public string JoinRoom(int roomId) 
        {
            JoinRoomRequest req = new JoinRoomRequest();
            req.roomId = roomId;

            //getting the name of the room the user wants to join
            Dictionary<string, int> rooms = GetRooms();
            if(rooms != null)
                foreach (string keyVar in rooms.Keys)
                    if (rooms[keyVar] == roomId)
                        m_roomName = keyVar;
            
            

            RequestInfo res = Communicate(Serilaizer.SerilizeRequest(req));
            if ((res.requestId == (int)CODES.ERROR_CODE))
            {
                m_roomName = "";
                return Deserilaizer.deserializeError(res.buffer).message;
            }

            return "";
        }

        /// <summary>
        /// function gets the statistics of the user in 3 part. 1 - the last games the user played. 
        /// 2 - the world record games. 3 - the personal stats of the user (times, amounts and stuff)
        /// </summary>
        /// <returns></returns>
        public StatisticsData GetStatistics() 
        {
            StatisticsData statictics = new StatisticsData();

            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_LAST_GAMES_CODE)); 
            statictics.last_games = Deserilaizer.deserializeLastGames(res.buffer).LastGames;

            res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_HIGH_SCORE_CODE));
            statictics.best_games = Deserilaizer.deserializeHighScore(res.buffer).HighScores;

            res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_PERSONAL_STATS_CODE));
            statictics.stats = Deserilaizer.deserializeStats(res.buffer).UserStatistics;

            return statictics;
        }
        public string[] GetPlayersInRoom() 
        { 
            int roomId = GetRooms()[m_roomName];
            GetPlayersInRoomRequest req = new GetPlayersInRoomRequest();
            req.roomId = roomId;

            RequestInfo res = Communicate(Serilaizer.SerilizeRequest(req));
            return Deserilaizer.deserializePlayersInRoom(res.buffer).PlayersInRoomes;
        }
        public Dictionary<string, int> GetRooms() 
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_ROOMS_CODE));
            return Deserilaizer.deserializeRooms(res.buffer).Rooms;
        }
        public bool LeaveRoom()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.LEAVE_ROOM_CODE));
            return res.requestId != (int)CODES.ERROR_CODE;
        }
        public bool CloseRoom()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.CLOSE_ROOM_CODE));
            return res.requestId != (int)CODES.ERROR_CODE;
        }
        public bool StartGame()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.START_GAME_CODE));
            return res.requestId != (int)CODES.ERROR_CODE;
        }
        public RoomsState GetRoomState()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_ROOM_STATE_CODE));
            roomState = Deserilaizer.deserializeRoomState(res.buffer).room_state;
            return roomState;
        }

        public RoomsState GetLastRoomState()
        {
            return roomState;
        }
        public bool LeaveGame()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.LEAVE_GAME_CODE));
            return res.requestId != (int)CODES.ERROR_CODE;
        }
        public int SubmitAnswer(Answer ans)
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest(ans));
            return Deserilaizer.deserializeSubmitAnswer(res.buffer).correctAnswerId;
        }
        public QuestionResponse GetQuestion()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_QUESTIONS_CODE));
            return Deserilaizer.deserializeQuestion(res.buffer);
        }
        public GameResultsResponse GetGameResults()
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest((int)CODES.GET_GAME_RESULTS_CODE));
            return Deserilaizer.deserializeGameResults(res.buffer);
        }

        public void AddQuestion(AddQuestionR info)
        {
            RequestInfo res = Communicate(Serilaizer.SerilizeRequest(info));
        }

        /// <summary>
        /// function send msg and returning the response from the server
        /// </summary>
        private RequestInfo Communicate(string data) {
            //sending the buffer with the request to the server
            byte[] buffer = new ASCIIEncoding().GetBytes(data);

            mutex.WaitOne();

            m_clientStream.Write(buffer, 0, buffer.Length);
            m_clientStream.Flush();

            //reciving the response code
            RequestInfo req_info = new RequestInfo();
            buffer = new byte[4096];
            int bytesRead = m_clientStream.Read(buffer, 0, 1);
            req_info.requestId = buffer[0];

            //reciving the response size
            bytesRead = m_clientStream.Read(buffer, 0, 4);
            int size = BitConverter.ToInt32(buffer);
            Array.Clear(buffer, 0, buffer.Length);

            //reciving the response
            bytesRead = m_clientStream.Read(buffer, 0, size);

            mutex.ReleaseMutex();
            req_info.buffer = System.Text.Encoding.UTF8.GetString(buffer);

            return req_info;
        }
        private Communicator()
        {
            m_client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 60000);

            m_client.Connect(serverEndPoint);
            m_clientStream = m_client.GetStream();
        }
          
    }
}
