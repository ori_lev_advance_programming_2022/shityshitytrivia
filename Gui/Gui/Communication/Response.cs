﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    public class StatusResponse
    {
        public int status { get; set; }
    }

    public class ErrorResponse
    {
        public int status { get; set; }
        public string message { get; set; }
    }

    public class GetRoomResponse
    {
        public int status { get; set; }
        public Dictionary<string, int> Rooms { get; set; }
    }

    public class GetPlayersInRoomResponse
    {
        public int status { get; set; }
        public string[] PlayersInRoomes { get; set; }
    }

    public class GetHighScoreResponse
    {
        public int status { get; set; }
        public GameSummery[] HighScores { get; set; }
    }

    public class GetLastGamesResponse
    {
        public GameSummery[] LastGames { get; set; }
        public int status { get; set; }
    }

    public class GetStatsResponse
    {
        public int status { get; set; }
        public Stats UserStatistics { get; set; }
    }

    public class Stats
    {
        public float average_answer_time { get; set; }
        public float total_of_games { get; set; }
        public float total_of_answers { get; set; }
        public float total_of_correct_answers { get; set; }
        public float correct_answers_rate { get; set; }
    }

    public class GetRoomStateResponse
    {
        public int status { get; set; }
        public RoomsState room_state { get; set; }
    }

    public class RoomsState
    {
        public int status { get; set; }
        public int answer_count { get; set; }
        public int answer_timeout { get; set; }
        public bool has_game_begun { get; set; }
        public string[] players { get; set; }
    }
    
   
    public class SubmitAnswerResponse
    {
		public int status { get; set; }
		public int correctAnswerId { get; set; }
	};

	public class QuestionResponse
	{
        public string[] answer { get; set; }
        public string question { get; set; }
        public int status { get; set; }
	}
    public class PlayerResults
    {
        public string username { get; set; }
        public int correctAnswerCount { get; set; }
        public int wrongAnswerCount { get; set; }
        public float averageAnswerCount { get; set; }
    }

    public class GameResultsResponse
    {
        public int status { get; set; }
        public PlayerResults[] results { get; set; }
    }
}
