﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Gui
{
    class Serilaizer
    {
        static private string BuildBuffer(int code, string json)
        {
            byte[] size = BitConverter.GetBytes(json.Length);
            string sizeStr = BitConverter.ToString(size);

            return (char)code + sizeStr + json;
        }
        static public string SerilizeRequest(int code)
        {
            return BuildBuffer(code, Communicator.getInstnace().m_userName);
        }
        static public string SerilizeRequest(CreateRoomRequest info) 
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.CREATE_ROOM_CODE, ans);
        }
        static public string SerilizeRequest(LoginRequest info) 
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.LOGIN_CODE, ans);
        }
        static public string SerilizeRequest(SignUpRequest info) 
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.SIGNUP_CODE, ans);
        }
        static public string SerilizeRequest(GetPlayersInRoomRequest info) 
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.GET_PLAYERS_IN_ROOM_CODE, ans);
        }
        static public string SerilizeRequest(JoinRoomRequest info) 
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.JOIN_ROOM_CODE, ans);
        }
        static public string SerilizeRequest(Answer info)
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.SUBMIT_ANSWER_CODE, ans);
        }
        static public string SerilizeRequest(AddQuestionR info)
        {
            string ans = JsonConvert.SerializeObject(info, Formatting.Indented);
            return BuildBuffer((int)Gui.CODES.ADD_QUESTION_CODE, ans);
        }
    }
}
