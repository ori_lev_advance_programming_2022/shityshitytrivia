import socket
import sys
import json


MIN_PORT = 1024
MAX_PORT = 65535

LOGIN_CODE = 2
SIGNUP_CODE = 3
LOGOUT_CODE = 4
CREATE_ROOM_CODE = 5
JOIN_ROOM_CODE = 6
GET_ROOMS_CODE = 7
GET_PLAYERS_IN_ROOM_CODE = 8
GET_HIGH_SCORE_CODE = 9
GET_LAST_GAMES_CODE = 10
GET_PERSONAL_STATS_CODE = 11

IP = "127.0.0.1"

BUFFER_LENGTH = 1024
HELLO_MESSAGE = "hello"

login_data = {
    "username": "g",
    "password": "123"
}

signup_data = {
    "username": "r",
    "password": "12345aA&",
    "mail": "wew",
    "addres" : "AAA, 123, AAA",
    "phone" : "055-6640991",
    "birth_date" : "11/05/2005"
}

logout = ""
get_rooms = ""
get_personal_state = ""
get_high_score = ""
get_last_game = ""


get_players_in_room_request = {
    "roomId": 1
}

join_room_request = {
    "roomId": 1
}

create_room_request = {
    "roomName": "Rabi David!",
    "maxUsers": 5,
    "questionCount": 8,
    "answerTimeout": 10
}

login_json = json.dumps(login_data)
signup_json = json.dumps(signup_data)
get_players_in_room_json = json.dumps(get_players_in_room_request)
join_room_json = json.dumps(join_room_request)
create_room_json = json.dumps(create_room_request)


json_by_code = {
    LOGIN_CODE: login_json,
    SIGNUP_CODE: signup_json,
    LOGOUT_CODE: logout,
    CREATE_ROOM_CODE: create_room_json,
    JOIN_ROOM_CODE: join_room_json,
    GET_ROOMS_CODE: get_rooms,
    GET_PLAYERS_IN_ROOM_CODE: get_players_in_room_json,
    GET_HIGH_SCORE_CODE: get_high_score,
    GET_LAST_GAMES_CODE: get_last_game,
    GET_PERSONAL_STATS_CODE: get_personal_state
}


def get_code() -> int:
    try:
        code = int(input("""Enter code.
LOGIN_CODE = 2
SIGNUP = 3
** ONLY WHEN LOG IN
LOGOUT_CODE = 4
CREATE_ROOM_CODE = 5
JOIN_ROOM_CODE = 6
GET_ROOMS_CODE = 7
GET_PLAYERS_IN_ROOM_CODE = 8
GET_HIGH_SCORE_CODE = 9
GET_LAST_GAMES_CODE = 10
GET_PERSONAL_STATS_CODE = 11
"""))

    except (ValueError, KeyboardInterrupt):
        print("Error: Invalid input")
        sys.exit()

    return code


def test_server(port: int) -> None:
    """
    Function test the server, in this current test the server running on the local machine.

    :param port: a port number to connect
    :return: None
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    address = (IP, port)
    sock.connect(address)
    while True:
        code = get_code()
        json_to_send = json_by_code[code]
        msg = code.to_bytes(1, 'big') + len(json_to_send).to_bytes(4, 'big') + bytes(json_to_send, 'ascii')
        #print(msg)
        #print(len(msg))
        try:
            sock.send(msg)
            print('Server Response:')
            #sock.recv(1)
            sock.recv(5)
            recvmsg = sock.recv(2048)
            print(recvmsg)
            msg = json.dumps(recvmsg.decode())
            print(msg, end='\n\n')

        except socket.error:
            print("Error: Couldn't connect.")
            sys.exit()


def get_port() -> int:
    """
    The function get port from the user and validate it, check if the port is valid.

    :return: the port number - int
    """
    try:
        port = int(input("Enter port: "))

        while port < MIN_PORT or port > MAX_PORT:
            print(f"Error, port range should be between {MIN_PORT}-{MAX_PORT}")
            port = int(input("Enter port: "))

    except (ValueError, KeyboardInterrupt):
        print("Error: Invalid input")
        sys.exit()

    return port


def main():
    port = 60000
    test_server(port)


if __name__ == '__main__':
    main()
